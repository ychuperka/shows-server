module.exports = Object.freeze({
    "NOT_FOUND": 404,
    "ACCESS_DENIED": 403,
    "BAD_REQUEST": 400,
    "INTERNAL_ERROR": 500
});