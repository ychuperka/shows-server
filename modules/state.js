var
    MySQLKVStorage = require("../classes/mysql_kvstorage"),
    ClientEventVote = require("../classes/client_events/vote");

var loaded = false,
    loggerInstance = null,
    container = {
        times: null,
        events: null,
        weights: null
    };

/**
 * Check state is loaded
 *
 * @return boolean
 */
function checkLoaded() {
    if (!loaded) {
        console.error("State not loaded");
        return false;
    }
    return true;
}

/**
 * Save times
 */
function saveTimes() {
    container.times.save({
        success: function () {
            loggerInstance.verbose("State", "Times saved");
        },
        failed: function (err) {
            loggerInstance.warn("State", "Can`t save times, error code %s, message: %s", err.code, err.message);
        }
    });
}

/**
 * Save events
 */
function saveEvents() {
    container.events.save({
        success: function () {
            loggerInstance.verbose("State", "Events saved");
        },
        failed: function (err) {
            loggerInstance.warn("State", "Can`t save events, error code %s, message: %s", err.code, err.message);
        }
    });
}

/**
 * Save weights
 */
function saveWeights() {
    container.weights.save({
        success: function () {
            loggerInstance.verbose("State", "Weights saved");
        },
        failed: function (err) {
            loggerInstance.warn("State", "Can`t save weights, error code %s, message: %s", err.code, err.message);
        }
    });
}

/**
 * Is vote type valid?
 * 
 * @param vote_type
 * @return {Boolean}
 */
function isVoteTypeValid(vote_type) {
    for (var vt in ClientEventVote.prototype.VOTE_TYPE) {
        if (vote_type != ClientEventVote.prototype.VOTE_TYPE[vt]) {
            continue;
        }
        return true;
    }
    return false;
}

/**
 * Convert KVStorage value to int
 * 
 * @param {KVStorage} kv_storage
 * @param {String} key
 */ 
function convertKVStorageValueToInt(kv_storage, key) {
    var value = kv_storage.get(key);
    if (!value) {
        value = 0;
    } else {
        value = parseInt(value);
    }
    kv_storage.set(key, value);
}

module.exports = {
	
	/**
	 * Load state
	 * 
	 * @param mysql_connection
	 * @param callbacks
	 */
    load: function (mysql_connection, logger, callbacks) {
        loggerInstance = logger;
        // Initialize events
        container.events = new MySQLKVStorage(mysql_connection, "events");
        container.events.load({
            success: function () {
                logger.verbose("State", "Events successfully loaded");
                // Initialize times
                container.times = new MySQLKVStorage(mysql_connection, "times");
                container.times.load({
                    success: function () {
                        logger.verbose("State", "Times successfully loaded");
                        convertKVStorageValueToInt(container.times, "sections_time");
                        convertKVStorageValueToInt(container.times, "shows_time");
                        // Initialize weights
                        container.weights = new MySQLKVStorage(mysql_connection, "weights");
                        container.weights.load({
                            success: function () {
                                logger.verbose("State", "Weights successfully loaded");
                                for (var vote_type in ClientEventVote.prototype.VOTE_TYPE) {
                                    var key = "vote_" + ClientEventVote.prototype.VOTE_TYPE[vote_type];
                                    convertKVStorageValueToInt(container.weights, key);
                                }
                                loaded = true;
                                callbacks.success();
                            },
                            failed: function (err) {
                                logger.warn("State", "Can`t load weights, error code %s, message %s", err.code, err.message);
                                callbacks.failed(err);
                            }
                        });
                    },
                    failed: function (err) {
                        logger.warn("State", "Can`t load times, error code %s, message %s", err.code, err.message);
                    }
                });
            },
            failed: function (err) {
                logger.warn("State", "Can`t load events, error code %s, message %s", err.code, err.message);
                callbacks.failed(err);
            }
        });
    },

	/**
	 * Set current event
	 * 
	 * @param type
	 * @param entity_id
     * @param state
	 */
    setCurrentEvent: function (type, entity_id, state) {
        if (!checkLoaded()) {
            return;
        }
        var entity_data = {
            "type": type,
            "entity_id": entity_id
        };
        if ((typeof state) !== "undefined") {
            entity_data.state = state;
        }
        container.events.set("current", JSON.stringify(entity_data));
        saveEvents();
    },
	
	/**
	 * Get current event
	 * 
	 * @return {}
	 */
    getCurrentEvent: function () {
        if (!checkLoaded()) {
            return;
        }
        var current = container.events.get("current");
        return current ? JSON.parse(container.events.get("current")) : {
            type: null,
            entity_id: null
        };
    },
    
    /**
     * Set event time
     * 
     * @param {Number} value
     */
    setEventTime: function (value) {
        if (!checkLoaded()) {
            return;
        }
        container.times.set("event_time", parseInt(value));
        saveTimes();
    },
    
    /**
     * Get event time
     * 
     * @return {Number}
     */
    getEventTime: function () {
        if (!checkLoaded()) {
            return;
        }
        var value = parseInt(container.times.get("event_time"));
        return value ? value : null;
    },
	
	/**
	 * Set sections time
	 * 
	 * @param value Milliseconds
	 */
    setSectionsTime: function (value) {
        if (!checkLoaded()) {
            return;
        }
        container.times.set("sections_time", parseInt(value));
        saveTimes();
    },
	
	/**
	 * Get sections time
	 * 
	 * @return string
	 */
    getSectionsTime: function () {
        if (!checkLoaded()) {
            return;
        }
        var value = parseInt(container.times.get("sections_time"));
        return value ? value : null;
    },
	
	/**
	 * Set shows time
	 * 
	 * @param value Milliseconds
	 */
    setShowsTime: function (value) {
        if (!checkLoaded()) {
            return;
        }
        container.times.set("shows_time", parseInt(value));
        saveTimes();
    },
	
	/**
	 * Get shows time
	 * 
	 * @return string
	 */
    getShowsTime: function () {
        if (!checkLoaded()) {
            return;
        }
        var value = parseInt(container.times.get("shows_time"));
        return value ? value : null;
    },
	
	/**
	 * Set last content update time
	 * 
	 * @param value Milliseconds
	 */
    setLastContentUpdateTime: function (value) {
        if (!checkLoaded()) {
            return;
        }
        container.times.set("last_content_update_time", parseInt(value));
        saveTimes();
    },
	
	/**
	 * Get last content update time
	 * 
	 * @return string
	 */
    getLastContentUpdateTime: function () {
        if (!checkLoaded()) {
            return;
        }
        var value = parseInt(container.times.get("last_content_update_time"));
        return value ? value : null;
    },
    
    
    setState: function (value) {
        if (!checkLoaded()) {
            return;
        }
        container.events.set("state", value);
        saveEvents();
    },
    
    getState: function () {
        if (!checkLoaded()) {
            return;
        }
        return container.events.get("state");
    },

    /**
     * Set vote type weight
     *
     * @param vote_type
     * @param weight
     * @throws {Error}
     */
    setVoteTypeWeight: function (vote_type, weight) {
        // Check vote type valid
        var valid = isVoteTypeValid(vote_type);
        if (!valid) {
            throw new Error("Unknown vote type \"%s\"", vote_type);
        }
        // Set weight
        weight = parseInt(weight);
        if (isNaN(weight)) {
            throw new Error("Invalid weight value");
        }
        var key = "vote_" + vote_type;
        container.weights.set(key, weight);
    },

    /**
     * Get vote type weight
     * 
     * @param vote_type
     * @throws {Error}
     * @return {Number|null}
     */
    getVoteTypeWeight: function (vote_type) {
        // Check vote type valid
        var valid = isVoteTypeValid(vote_type);
        if (!valid) {
            throw new Error("Unknown vote type \"%s\"", vote_type);
        }
        // Get weight
        return container.weights.get("vote_" + vote_type);
    },

    saveVoteTypesWeights: function (callbacks) {
        container.weights.save({
            success: function () {
                loggerInstance.verbose("State", "Vote types weights saved");
                callbacks.success();
            },
            failed: function (err) {
                loggerInstance.warn("State", "Can`t save vote types weights, error code: %s, message: %s", err.code, err.message);
                callbacks.failed(err);
            }
        });
    }
};