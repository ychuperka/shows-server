var read = require("fs").readSync,
    exists = require("fs").existsSync,
    resolve = require("path").resolve;

/**
 *
 * Configuration
 *
 * @param logger
 * @param shutdown_cb
 * @constructor
 */
function Config (logger, shutdown_cb) {

    if (!(this instanceof Config)) {
        return new Config(logger);
    }

    this.logger = logger;

    /**
     * Loads configuration file
     *
     * @param path
     * @returns {*}
     */
    this.load = function (path) {
        if (!exists(path)) {
            this.logger.error("Config", "Configuration file not found, path \"%s\"", path);
            if (shutdown_cb) {
                shutdown_cb(1);
            }
            return null;
        }

        this.logger.verbose("Config", "Loading configuration file \"%s\"", path);
        return require(path);
    };

    this.db = this.load(
        resolve(__dirname, "..", "config", "db.json")
    );

    this.app = this.load(
        resolve(__dirname, "..", "config", "app.json")
    );
}

module.exports = Config;