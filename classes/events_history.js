var
    ClientEventVote = require("./client_events/vote");

/**
 * @param di_container {DIContainer}
 * 
 * @class
 * @constructor
 */
function EventsHistory(di_container) {
    this.di_container = di_container;
    this.should_update = true;
    this.data = null;
};
module.exports = EventsHistory;

EventsHistory.prototype.GROUP = Object.freeze({
    SECTIONS: 0,
    SHOWS: 1
});

/**
 * Convert group id to string
 * 
 * @param group_id
 * @private
 * @return {(string|null)}
 */
EventsHistory.prototype._convertGroupIdToString = function (group_id) {
    for (var prop in this.GROUP) {
        var id = this.GROUP[prop];
        if (id == group_id) {
            return prop.toLowerCase();
        }
    }
    return null;
};

/**
 * Convert group id to string and check group exists in a content and get group name
 * 
 * @param group_id
 * @private
 * @return {(string|null)}
 */
EventsHistory.prototype._convertGroupIdToStringAndCheckGroupExistsInContentAndGetGroupName = function (group_id) {
    var
        logger = this.di_container.get("logger"),
        content = this.di_container.get("content"),
        group = this._convertGroupIdToString(group_id);
    if (group === null) {
        logger.error("History", "Unknown group \"%s\"", group_id);
        return null;
    }
    if (!content.hasOwnProperty(group)) {
        logger.error("History", "Group \"%s\" not found in content", group);
        return null;
    }
    return group;
};

/**
 * Check entity exists
 * 
 * @param {number} group
 * @param {number} entity_id
 * 
 * @return {boolean}
 */
EventsHistory.prototype._entityExists = function (group, entity_id) {
    var
        logger = this.di_container.get("logger"),
        content = this.di_container.get("content");
    // Check entity exists
    var found = false;
    content[group].every(function (item) {
        if (item.id == entity_id) {
            found = true;
            return false;
        }
        return true;
    });
    if (!found) {
        logger.error("History", "Entity \"%s\" not found in group \"%s\"", entity_id, group);
        return false;
    }
    return true;
};

EventsHistory.prototype.add = function (group_id, entity_id) {
    var
        instance = this,
        mysql_connection = this.di_container.get("mysql_connection"),
        logger = this.di_container.get("logger");
    // Check group exists
    var group = this._convertGroupIdToStringAndCheckGroupExistsInContentAndGetGroupName(group_id);
    if (group === null) {
        return;
    }
    // Check entity exists
    if (!this._entityExists(group, entity_id)) {
        return;
    }
    // Add record
    mysql_connection.query(
        "INSERT INTO `history` (`group_id`, `entity_id`, `started_at`) "
        + "VALUES (?, ?, NOW())",
        [group_id, entity_id],
        function (err) {
            if (err) {
                logger.warn("History", "Can`t add history item, error code \"%s\", message: %s", err.code, err.message);
                return;
            }
            instance.should_update = true;
        }
        );
};

EventsHistory.prototype.callIfRecordExists = function (group_id, entity_id, cb) {
    var
        mysql_connection = this.di_container.get("mysql_connection"),
        logger = this.di_container.get("logger");
    // Check group exists
    var group = this._convertGroupIdToStringAndCheckGroupExistsInContentAndGetGroupName(group_id);
    if (group === null) {
        return;
    }
    // Check entity exists
    if (!this._entityExists(group, entity_id)) {
        return;
    }
    // Check record exists
    mysql_connection.query(
        "SELECT `id` FROM `history` WHERE `group_id` = ? AND `entity_id` = ? ORDER BY `id` DESC LIMIT 1",
        [group_id, entity_id],
        function (err, results) {
            if (err) {
                logger.warn("History", "Can`t select history records, error code \"%s\", message: %s", err.code, err.message);
                return;
            }
            if (results.length == 0) {
                logger.warn("History", "Record not found, group id: %s, entity id: %s", group_id, entity_id);
                return;
            }
            var record_id = results[0].id;
            cb(record_id);
        }
        );
};

EventsHistory.prototype.markAsStopped = function (group_id, entity_id) {
    var
        instance = this,
        mysql_connection = this.di_container.get("mysql_connection"),
        logger = this.di_container.get("logger");
    // Mark record as stopped	
    this.callIfRecordExists(group_id, entity_id, function (record_id) {
        mysql_connection.query(
            "UPDATE `history` SET `stopped_at` = NOW() WHERE `id` = ?",
            [record_id],
            function (err) {
                if (err) {
                    logger.warn("History", "Can`t mark record as stopped, error code \"%s\", message: %s", err.code, err.message);
                    return;
                }
                instance.should_update = true;
            }
            );
    });
};

EventsHistory.prototype.setWinnerId = function (section_id, value) {
    var
        instance = this,
        mysql_connection = this.di_container.get("mysql_connection"),
        content = this.di_container.get("content"),
        logger = this.di_container.get("logger");
    // Check winner exists
    if (!content.hasOwnProperty("nominees")) {
        logger.error("History", "Nominees list not found");
        return;
    }
    if (!this._entityExists("nominees", value)) {
        return;
    }	
    // Set winner id		
    this.callIfRecordExists(this.GROUP.SECTIONS, section_id, function (record_id) {
        mysql_connection.query(
            "UPDATE `history` SET `winner_id` = ? WHERE `id` = ?",
            [value, record_id],
            function (err) {
                if (err) {
                    logger.warn("History", "Can`t set winner id, error code \"%s\", message: %s", err.code, err.message);
                    return;
                }
                logger.verbose("Server", "Winner #%s set for section #%s", value, section_id);
                instance.should_update = true;
            }
            );
    });
};

EventsHistory.prototype.setPlace = function (show_id, value) {
    var
        instance = this,
        mysql_connection = this.di_container.get("mysql_connection"),
        logger = this.di_container.get("logger");
    // Set place 
    this.callIfRecordExists(this.GROUP.SHOWS, show_id, function (record_id) {
        mysql_connection.query(
            "UPDATE `history` SET `place` = ? WHERE `id` = ?",
            [value, record_id],
            function (err) {
                if (err) {
                    logger.warn("History", "Can`t set place, error code \"%s\", message: %s", err.code, err.message);
                    return;
                }
                instance.should_update = true;
            }
            );
    });
};

EventsHistory.prototype.getPlace = function (show_id, success_cb, fail_cb) {
    var
        logger = this.di_container.get("logger"),
        mysql_connection = this.di_container.get("mysql_connection");
    // Get place
    mysql_connection.query(
        "SELECT `place` FROM `history` WHERE `group_id` = ? AND `entity_id` = ?",
        [this.GROUP.SHOWS, show_id],
        function (err, results) {
            if (err) {
                logger.error("Server", "Can`t select place data, error code \"%s\", message: %s", err.code, err.message);
                fail_cb(err);
                return;
            }
            var place = null;
            if (results.length == 0) {
                logger.warn("Server", "Place data for show \"%s\" not found", show_id);
            } else {
                place = results[0].place;
            }
            success_cb(place);
        }
        );
};

EventsHistory.prototype._convertSelectionResultToSection = function (result) {
    return {
        id: result.entity_id,
        type: 'section',
        winner_id: result.winner_id,
        started_at: result.started_at,
        stopped_at: result.stopped_at
    };
};

EventsHistory.prototype._convertSelectionResultToShow = function (result) {
    return {
        id: result.entity_id,
        type: 'show',
        // place: result.place,
        started_at: result.started_at,
        stopped_at: result.stopped_at
    };
};

EventsHistory.prototype._getHistoryFromDatabase = function (cb) {
    var
        instance = this,
        logger = this.di_container.get("logger"),
        mysql_connection = this.di_container.get("mysql_connection");
    mysql_connection.query("SELECT * FROM `history`", function (err, results) {
        if (err) {
            logger.warn("History", "Can`t select history data, error code \"%s\", message: %s", err.code, err.message);
            cb([]);
            return;
        }
        // Prepare result
        var result = [];
        results.forEach(function (row) {
            switch (row.group_id) {
                case instance.GROUP.SECTIONS:
                    result.push(instance._convertSelectionResultToSection(row));
                    break;
                case instance.GROUP.SHOWS:
                    result.push(instance._convertSelectionResultToShow(row));
                    break;
                default:
                    logger.error("History", "Unknown group id \"%s\"", row.group_id);
                    cb([]);
                    break;
            }
        });
        // Call callback
        instance.data = result;
        instance.should_update = false;
        cb(result);
    });
}

EventsHistory.prototype._getVotesForShowsInAHistory = function (history, cb) {
    var
        logger = this.di_container.get("logger"),
        mysql_connection = this.di_container.get("mysql_connection");
    // Get shows ids
    var shows_ids = [];
    history.forEach(function (item) {
        if (item.type != 'show') {
            return;
        }
        shows_ids.push(item.id);
    });
    // Get votes data
    mysql_connection.query(
        "SELECT `show_id`, `type` FROM `vote` WHERE `show_id` IN (?)",
        [shows_ids],
        function (err, results) {
            if (err) {
                logger.error("History", "Can`t select votes data");
                cb(history);
                return;
            }
            if (results.length == 0) {
                logger.warn("History", "Votes data is empty");
                cb(history);
                return;
            }
            // Prepare votes data
            var votes_data = {};
            results.forEach(function (item) {
                var index = null;
                switch (item.type) {
                    case ClientEventVote.prototype.VOTE_TYPE.GOOD:
                        index = 0;
                        break;
                    case ClientEventVote.prototype.VOTE_TYPE.VERY_GOOD:
                        index = 1;
                        break;
                    case ClientEventVote.prototype.VOTE_TYPE.EXCELLENT:
                        index = 2;
                        break;
                    default:
                        logger.error("History", "Unknown vote type \"%s\"", item.type);
                        break;
                }
                if (index === null) {
                    return;
                }    
                // Check property exists
                if (!votes_data.hasOwnProperty(item.show_id)) {
                    votes_data[item.show_id] = [0, 0, 0];
                }
                // Increment array value on selected index
                votes_data[item.show_id][index]++;
            });
            // Put votes data to the history
            history.forEach(function (item) {
                if (item.type != "show") {
                    return;
                }
                if (!votes_data.hasOwnProperty(item.id)) {
                    item["votes"] = [0, 0, 0];
                } else {
                    item["votes"] = votes_data[item.id];
                }
            });
            // Call callback
            cb(history);
        }
        );
};

EventsHistory.prototype.clear = function () {
    this.should_update = true;
    this.data = null;
    var self = this;
    this.di_container.get("mysql_connection")
        .query("TRUNCATE TABLE `history`", function (err) {
            var logger = self.di_container.get("logger");
            if (err) {
                logger.error("History", "Can`t truncate history table, error code: %s, message: %s", err.code, err.message);
                return;
            }
            logger.info("History", "History is cleared");
        });
};

EventsHistory.prototype.toObject = function (cb) {
    if (this.should_update) {
        // var self = this;
        this._getHistoryFromDatabase(function (history) {
            // self._getVotesForShowsInAHistory(history, cb);
            cb(history); // Comment the line if uncomment line before
        });
    } else {
        cb(this.data);
    }
};