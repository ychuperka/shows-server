/**
 * @constructor
 */
function KVStorage () {
    this.container = {};
}
module.exports = KVStorage;


/**
 * Sets key value.
 * 
 * @param key
 * @param value
 */
KVStorage.prototype.set = function (key, value) {
    this.container[key] = value;
};

/**
 * Check pair exists
 * 
 * @param key
 */
KVStorage.prototype.exists = function (key) {
    return this.container.hasOwnProperty(key);  
};

/**
 * Remove pair if exists.
 * Return false if pair not exists else 
 * return true.
 * 
 * @param key
 */
KVStorage.prototype.remove = function (key) {
    if (!this.exists(key)) {
        return false;
    }  
    delete this.container[key];
    return true;
};

/**
 * Get pair value by key
 *
 * @pair key
 */
KVStorage.prototype.get = function (key) {
    if (!this.exists(key)) {
        return undefined;
    }  
    return this.container[key];
};

/**
 * Overrides Object.toString to return
 * JSON representation for property "container"
 * 
 * @return String
 */
KVStorage.prototype.toString = function () {
    return JSON.stringify(this.container);
}

/**
 * Execute callback for each container element
 */
KVStorage.prototype.forEach = function (cb) {
    for (var prop in this.container) {
        cb(prop, this.container[prop]);
    }
}
