var
    ClientEvent = require("../client_event"),
    ErrorCodes = require("../../modules/error_codes");
	
/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io socket
 * @param data {Object}
 * 
 * @constructor
 */
function ClientEventVote(di_container, socket, data) {
    ClientEvent.apply(this, arguments);
};
ClientEventVote.prototype = Object.create(ClientEvent.prototype);
ClientEventVote.prototype.constructor = ClientEventVote;
module.exports = ClientEventVote;

/*
Possible vote types
*/
ClientEventVote.prototype.VOTE_TYPE = Object.freeze({
    GOOD: 0,
    VERY_GOOD: 1,
    EXCELLENT: 2
});

/**
 * Run the event
 */
ClientEventVote.prototype.run = function () {
    // Check request empty
    if (ClientEvent.prototype._emitEventIfRequestEmpty.call(this, "vote-failed")) {
        return;
    }
    this.di_container.get("logger")
        .verbose("Server", "Trying to make a vote, socket %s", this.socket.id);
    this._makeVote();
};

/**
 * Checks show_id 
 * 
 * @return boolean
 */
ClientEventVote.prototype._isVoteShowIdValid = function () {
    if (ClientEvent.prototype._emitEventIfValueUndefined.call(this, "vote-failed", "show_id", this.data.show_id)) {
        return false;
    }
    var show_id = parseInt(this.data.show_id);
    if (isNaN(show_id)) {
        this.di_container.get("logger")
            .warn("Server", "Invalid show id, socket %s", this.socket.id);
        this.socket.emit("vote-failed", {
            error: {
                code: ErrorCodes.BAD_REQUEST,
                message: "Invalid show id"
            }
        });
        return false;
    }

    return true;
};

/**
 * Is vote type valid?
 * 
 * @return boolean
 */
ClientEventVote.prototype._isVoteTypeValid = function () {
    /*
    Check type
     */
    if (ClientEvent.prototype._emitEventIfValueUndefined.call(this, "vote-failed", "vote_type", this.data.vote_type)) {
        return false;
    }

    var result = false;
    for (var p in this.VOTE_TYPE) {
        if (this.VOTE_TYPE[p] === this.data.vote_type) {
            result = true;
            break;
        }
    }

    if (!result) {
        this.di_container.get("logger").warn("Server", "Invalid vote type, socket %s", this.socket.id);
        this.socket.emit("vote-failed", {
            error: {
                code: ErrorCodes.BAD_REQUEST,
                message: "Invalid vote type"
            }
        });
    }

    return result;
};

/**
 * Call the callback if show exists and has valid type
 * 
 * @param cb Callback to call
 */
ClientEventVote.prototype._callIfShowExistsAndHasValidType = function (cb) {
    var
        data = this.data,
        socket = this.socket,
        state = this.di_container.get("state"),
        logger = this.di_container.get("logger"),
        content = this.di_container.get("content");
    // Check shows list exists
    if (!content.hasOwnProperty("shows")) {
        logger.error("Server", "Shows list not found in content, socket id %s", socket.id);
        socket.emit("vote-failed", {
            show_id: data.show_id,
            error: {
                code: ErrorCodes.INTERNAL_ERROR,
                message: "Shows list not found in content"
            }
        });
        return;
    }    
    // Check entity exists
    if (!ClientEvent.prototype._entityExists.call(this, content.shows, data.show_id)) {
        logger.warn("Server", "Show \"%s\" not found, socket id %s", data.show_id, socket.id);
        socket.emit("vote-failed", {
            show_id: data.show_id,
            error: {
                code: ErrorCodes.NOT_FOUND,
                message: "Show \"" + data.show_id + "\" not found"
            }
        });
        return;
    }
    // Check show is active
    var current_event = state.getCurrentEvent();
    if (current_event.type !== "show" || current_event.entity_id != data.show_id) {
        logger.warn("Server", "Attempt to send vote for innactive show \"%s\", socket id %s", data.show_id, socket.id);
        socket.emit("vote-failed", {
            show_id: data.show_id,
            error: {
                code: ErrorCodes.ACCESS_DENIED,
                message: "Attempt to send vote for innactive show"
            }
        });
        return;
    }
    // Check show has a valid type
    if (!current_event.hasOwnProperty("state") || current_event.state !== "voting") {
        logger.warn("Server", "Attempt to send vote for show that is not in valid state, socket %s", socket.id);
        socket.emit("vote-failed", {
            show_id: data.show_id,
            error: {
                code: ErrorCodes.ACCESS_DENIED,
                message: "Attempt to send vote for show that is not in valid state"
            }
        });
        return;
    }
    // All checks passed
    cb();
};

/**
 * Call callback if user exists and votes first time
 * 
 * @param {Callback} cb Callback to call
 */
ClientEventVote.prototype._callIfUserExistsAndVotesFirstTime = function (cb) {
    // Check user id specified
    if (!this.data.hasOwnProperty("user_id")) {
        this.di_container.get("logger")
            .warn("Server", "User id not specified, socket id %s", this.socket.id);
        this.socket.emit("vote-failed", {
            error: {
                code: ErrorCodes.BAD_REQUEST,
                message: "User id not specified"
            }
        });    
        return;
    }
    /*
    Check user exists
    */
    var eventInstance = this;
    this.di_container.get("mysql_connection").query(
        "SELECT COUNT(`id`) AS `count` FROM `user` WHERE `id` = ?",
        [this.data.user_id],
        function (err, results) {
            if (err) {
                eventInstance._emitDbErrorEvent("vote-failed", err);
                return;
            }
            if (results.length == 0) {
                eventInstance.di_container.get("logger")
                    .warn("Server", "User #%s not found, socket %s", this.data.user_id, eventInstance.socket.id);
                eventInstance.socket.emit("vote-failed", {
                    error: {
                        code: ErrorCodes.NOT_FOUND,
                        message: "The user not found"
                    }
                });
                return;
            }
            /*
            Check user votes first time
            */
            eventInstance.di_container.get("mysql_connection").query(
                "SELECT COUNT(`id`) AS `count` FROM `vote` WHERE `user_id` = ? AND `show_id` = ?",
                [eventInstance.data.user_id, eventInstance.data.show_id],
                function (err, results) {
                    if (err) {
                        eventInstance._emitDbErrorEvent("vote-failed", err);
                        return;
                    }
                    if (results[0].count > 0) {
                        eventInstance.di_container.get("logger")
                            .warn("Server", "Vote already sent, socket %s", eventInstance.socket.id);
                        eventInstance.socket.emit("vote-failed", {
                            show_id: eventInstance.data.show_id,
                            error: {
                                code: ErrorCodes.ACCESS_DENIED,
                                message: "Vote already sent"
                            }
                        });
                        return;
                    }
                    /*
                    All checks passed
                    */
                    cb();
                });
        });
};

/**
 * Call callback if successfully add a vote
 * 
 * @param cb Callback to call
 */
ClientEventVote.prototype._callIfAddVoteSuccess = function (cb) {
    /*
    Add a vote
    */
    var eventInstance = this;
    this.di_container.get("mysql_connection").query(
        "INSERT INTO `vote` (`user_id`, `show_id`, `type`) VALUES (?, ?, ?)",
        [this.data.user_id, this.data.show_id, this.data.vote_type],
        function (err) {

            if (err) {
                eventInstance._emitDbErrorEvent("vote-failed", err);
                return;
            }

            cb();
        });
};

/**
 * Make a vote
 * 
 */
ClientEventVote.prototype._makeVote = function () {

    if (!this._isVoteShowIdValid()) {
        return;
    }

    if (!this._isVoteTypeValid()) {
        return;
    }

    var eventInstance = this;
    this._callIfShowExistsAndHasValidType(function () {
        eventInstance._callIfUserExistsAndVotesFirstTime(function () {
            eventInstance._callIfAddVoteSuccess(function () {
                eventInstance.di_container.get("logger")
                    .verbose("Server", "Vote added successfully, socket %s", eventInstance.socket.id);
                eventInstance.socket.emit("vote-success", {
                    show_id: eventInstance.data.show_id
                });
            });
        });
    });
};
