var
    ClientEvent = require("../client_event"),
    ErrorCodes = require("../../modules/error_codes");
	
/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io socket
 * @param data {Object}
 * 
 * @constructor
 */
function ClientEventSync(di_container, socket, data) {
	ClientEvent.apply(this, arguments);
}
ClientEventSync.prototype = Object.create(ClientEvent.prototype);
ClientEventSync.prototype.constructor = ClientEventSync;
module.exports = ClientEventSync;

ClientEventSync.prototype.run = function () {
	var
		socket = this.socket,
		logger = this.di_container.get("logger"),
		state = this.di_container.get("state");
	logger.verbose("Server", "Socket %s asks to sync", this.socket.id);
	this.di_container.get("history").toObject(function (history) {
		socket.emit("sync", {
            state: state.getState(),
			values: {
				server: (new Date).getTime(),
				section: state.getSectionsTime(),
				show: state.getShowsTime(),
                event: state.getEventTime(),
				update: state.getLastContentUpdateTime()
			},
			events: {
				"history": history,
				current: state.getCurrentEvent()
			}
		});
	});
};