var
    crypto = require("crypto"),
    ClientEvent = require("../client_event"),
    AdminEventSync = require("../admin_events/sync"),
    ErrorCodes = require("../../modules/error_codes");

/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io socket
 * @param data {Object}
 * 
 * @constructor
 */
function ClientEventUserRegistration(di_container, socket, data) {
    ClientEvent.apply(this, arguments);
    this.validation_bad_fields = [];
}
ClientEventUserRegistration.prototype = Object.create(ClientEvent.prototype);
ClientEventUserRegistration.prototype.constructor = ClientEventUserRegistration;
module.exports = ClientEventUserRegistration;

/**
 * Run the event
 * 
 */
ClientEventUserRegistration.prototype.run = function () {
    var logger = this.di_container.get("logger"),
        mysql_connection = this.di_container.get("mysql_connection");
    logger.verbose("Server", "Trying to register a user, socket %s", this.socket.id);
    // Check request empty
    if (ClientEvent.prototype._emitEventIfRequestEmpty.call(this, "user-registration-failed")) {
        return;
    }
    // Check password
    if (!this._isPasswordValid()) {
        logger.warn("Server", "Wrong user password from %s", this.socket.id);
        this.socket.emit("user-registration-failed", {
            "error": {
                "code": ErrorCodes.ACCESS_DENIED,
                "message": "Wrong password"
            }
        });
        return;
    }    
    // Validate user`s data
    if (!this._isDataValid()) {
        logger.verbose("Server", "Invalid user data from %s", this.socket.id);
        this.socket.emit("user-registration-failed", {
            "fields": this.validation_bad_fields,
            "error": {
                "code": ErrorCodes.BAD_REQUEST,
                "message": "Invalid data"
            }
        });
        return;
    }
    /*
    Try to register user
    */
    var user_data = {
        "name": this.data.name.trim(),
        "surname": this.data.surname.trim()
    };
    // Select user id by name and surname
    var instance = this;
    mysql_connection.query("SELECT `id` FROM `user` WHERE `name` = ? AND `surname` = ?",
        [user_data.name, user_data.surname],
        function (err, results) {
            if (err) {
                ClientEvent.prototype._emitDbErrorEvent.call(instance, err);
                return;
            }
            var processDbResult = function (err, user_id) {
                if (err) {
                    ClientEvent.prototype._emitDbError.call(instance, err);
                } else {
                    instance.socket.emit("user-registration-success", {
                        "name": user_data.name,
                        "surname": user_data.surname,
                        "user_id": user_id
                    });
                    // Sync with admin
                    (new AdminEventSync(
                        instance.di_container,
                        instance.di_container.get("io_ns_admin")
                        )).run(false);
                    logger.verbose(
                        "Server", "User registration success, socket %s",
                        instance.socket.id
                        );
                }
            };
            // If user found
            if (results.length > 0) {
                // If token specified
                if (instance.data.hasOwnProperty("token")) {
                    // Update token
                    mysql_connection.query(
                        "UPDATE `user` SET `token` = ? WHERE `id` = ?",
                        [instance.data.token.trim(), results[0].id],
                        function (err) {
                            processDbResult(err, results[0].id);
                        }
                        );
                } else {
                    // Just send user data
                    processDbResult(null, results[0].id);
                }
            } else {
                // User not found, insert user data
                var token = instance.data.hasOwnProperty("token") ? instance.data.token.trim() : null;
                mysql_connection.query(
                    "INSERT INTO `user` (`name`, `surname`, `token`) VALUES (?, ?, ?)",
                    [user_data.name, user_data.surname, token],
                    function (err, result) {
                        processDbResult(err, result.insertId);
                    });
            }
        });
};

/**
 * Check password is valid
 * 
 * @return {Boolean}
 */
ClientEventUserRegistration.prototype._isPasswordValid = function () {
    var result = true,
        config = this.di_container.get("config");
    if (!this.data.hasOwnProperty("password")) {
        result = false;
    } else {
        var sha1 = crypto.createHash("sha1");
        sha1.update(config.app.password);
        var hash = sha1.digest("hex");
        result = hash === this.data.password;
    }
    return result;
}

/**
 * Is provided data valid?
 * 
 * @return boolean
 */
ClientEventUserRegistration.prototype._isDataValid = function () {

    this.validation_bad_fields.length = 0;
    var result = true,
        config = this.di_container.get("config");

    var validation_patterns = config.app.validation_patterns.user;
    for (var prop in validation_patterns) {
        // Get pattern data
        var pattern_data = validation_patterns[prop];
        // If field is required but not specified then it is invalid
        if (pattern_data.required && !this.data.hasOwnProperty(prop)) {
            this.validation_bad_fields.push(prop);
            if (result) {
                result = false;
            }
            continue;
        } else if (!pattern_data.required && !this.data.hasOwnProperty(prop)) {
            continue;
        }

        var regexp = new RegExp(validation_patterns[prop].pattern);
        if (!regexp.test(this.data[prop])) {
            this.validation_bad_fields.push(prop);
            if (result) {
                result = false;
            }
        }
    }

    return result;
};
