var
	KVStorage = require("./kvstorage");

/**
 * 
 * @param mysql_connection
 * @param group
 * 
 * @constructor
 */
function MySQLKVStorage (mysql_connection, group) {
	KVStorage.apply(this, arguments);
	this.mysql_connection = mysql_connection;
	this.group = group;
}
MySQLKVStorage.prototype = Object.create(KVStorage.prototype);
MySQLKVStorage.prototype.constructor = MySQLKVStorage;
module.exports = MySQLKVStorage;

/**
 * Load data
 * 
 * @param @param callbacks {success: function ()..., failed: function (err)...}
 */
MySQLKVStorage.prototype.load = function (callbacks) {
	var instance = this;
	this.mysql_connection.query("SELECT * FROM `kv_storage` WHERE `group` = ?", 
		[this.group], function (err, results) {
			if (err) {
				callbacks.failed(err);
				return;
			}
			results.forEach(function (row) {
				KVStorage.prototype.set.call(instance, row.key, row.value);
			});
			callbacks.success();
		});
};

/**
 * Save data
 * 
 * @param callbacks {success: function ()..., failed: function (err)...}
 */
MySQLKVStorage.prototype.save = function (callbacks) {
	var instance = this;
	// Delete all record for the current group
	this.mysql_connection.query("DELETE FROM `kv_storage` WHERE `group` = ?", [this.group], function (err) {
		if (err) {
			callbacks.failed(err);
			return;
		}
		// Build SQL query to insert all data 
		var params = [];
		var parts = [];
		KVStorage.prototype.forEach.call(instance, function (key, value) {
			parts.push("(?, ?, '" + instance.group + "')");
			params.push(key);
			params.push(value);
		});
		var sql = "INSERT INTO `kv_storage` (`key`, `value`, `group`) VALUES "
			+ parts.join(", ");
		// Execute query to insert data	
		instance.mysql_connection.query(sql, params, function (err) {
			if (err) {
				callbacks.failed(err);
				return;
			}
			callbacks.success();
		});	
	});
};