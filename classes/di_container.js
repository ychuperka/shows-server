var
	KVStorage = require("./kvstorage");

/**
 * 
 * @constructor
 */
function DIContainer() {
	KVStorage.apply(this, arguments);
}
DIContainer.prototype = Object.create(KVStorage.prototype);
DIContainer.prototype.constructor = DIContainer;
module.exports = DIContainer;

/**
 * 
 * @param key
 */
DIContainer.prototype.get = function (key) {
	var value = KVStorage.prototype.get.call(this, key);
	if ((typeof value) === "undefined") {
		throw new Error("Value for key \"" + key + "\" not found");
	}	
	return value;
};
