var
    AdminEvent = require("../admin_event");

function AdminEventSyncClients(di_container, socket) {
    AdminEvent.apply(this, arguments);
}
AdminEventSyncClients.prototype = Object.create(AdminEvent.prototype);
AdminEventSyncClients.prototype.constructor = AdminEventSyncClients;
module.exports = AdminEventSyncClients;

AdminEventSyncClients.prototype.run = function () {
    var self = this,
        logger = this.di_container.get("logger"),
        state = this.di_container.get("state");
    // Check admin logged in
    if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-sync-clients-failed")) {
        logger.warn("Server", "Unauthorized attempt to sync from admin");
        return;
    }

    logger.info("Server", "Force clients synchronization")

    // Sync
    this.di_container.get("history").toObject(function (history) {
        self.di_container.get("io_ns_client").emit("sync", {
            state: state.getState(),
            values: {
                server: (new Date).getTime(),
                section: state.getSectionsTime(),
                show: state.getShowsTime(),
                event: state.getEventTime(),
                update: state.getLastContentUpdateTime()
            },
            events: {
                "history": history,
                current: state.getCurrentEvent()
            }
        });
    });

    this.socket.emit("admin-sync-clients-success");
};