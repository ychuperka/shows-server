var
    ErrorCodes = require("../../modules/error_codes"),
    AdminEvent = require("../admin_event");

function AdminEventTruncateTables(di_container, socket) {
    AdminEvent.apply(this, arguments);
}
AdminEventTruncateTables.prototype = Object.create(AdminEvent.prototype);
AdminEventTruncateTables.prototype.constructor = AdminEventTruncateTables;
module.exports = AdminEventTruncateTables;

AdminEventTruncateTables.prototype.run = function () {
    var
        self = this,
        logger = this.di_container.get("logger"),
        mysql_connection = this.di_container.get("mysql_connection");
    // Truncate tables    
    var tables = [
        'vote', 'kv_storage'
    ];
    tables.forEach(function (tbl, index) {
        (function (is_last) {
            mysql_connection.query("TRUNCATE TABLE `" + tbl + "`", function (err) {
                if (err) {
                    logger.error("Server", "Can`t truncate table: %s", tbl);
                    self.socket.emit("admin-truncate-tables-failed", {
                        error: {
                            code: ErrorCodes.INTERNAL_ERROR,
                            message: "Can`t truncate table: " + tbl
                        }
                    });
                    return;
                }
                if (is_last) {
                    logger.info("Server", "Tables truncated");
                    logger.info("Server", "Reloading state");
                    var state = self.di_container.get("state");
                    var upd_time = state.getLastContentUpdateTime();
                    self.di_container.get("state")
                        .load(mysql_connection, logger, {
                            success: function () {
                                logger.info("Server", "State reloaded");
                                self.socket.emit("admin-truncate-tables-success");
                                state.setLastContentUpdateTime(upd_time);
                            },
                            failed: function (err) {
                                logger.error("Server", "State not reloaded");
                                self.socket.emit("admin-truncate-tables-failed", {
                                    error: {
                                        code: ErrorCodes.INTERNAL_ERROR,
                                        message: err.message
                                    }
                                });
                            }
                        });
                }
            });
        })((index + 1) == tables.length);
    });
    // Clear history
    this.di_container.get("history").clear();
};