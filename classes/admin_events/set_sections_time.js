var
	ErrorCodes = require("../../modules/error_codes"),
	AdminEvent = require("../admin_event");
	
/**
 * 
 * @param di_container {DICointainer}
 * @param socket socket.io socket
 * @param data {Object}
 * 
 * @constructor
 */
function AdminEventSetSectionsTime(di_container, socket, data) {
	AdminEvent.apply(this, arguments);
}
AdminEventSetSectionsTime.prototype = Object.create(AdminEvent.prototype);
AdminEventSetSectionsTime.prototype.constructor = AdminEventSetSectionsTime;
module.exports = AdminEventSetSectionsTime;

AdminEventSetSectionsTime.prototype.run = function () {
	var
		logger = this.di_container.get("logger"),
		socket = this.socket,
		data = this.data;
	// Check request empty
	if (AdminEvent.prototype._emitEventIfRequestEmpty.call(this, "admin-set-sections-time-failed")) {
		return;
	}	
    // Check admin logged in
    if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-set-sections-time-failed")) {
        logger.warn("Server", "Unauthorized attempt to set sections time, socket id %s", socket.id);
        return;
    }
	// Check time value specified
	if (!data.hasOwnProperty("value")) {
		logger.warn("Server", "Time value not specified, socket id %s", socket.id);
		socket.emit("admin-set-sections-time-failed", {
			error: ErrorCodes.BAD_REQUEST,
			message: "Time value not specified"
		});
		return;
	}
	// TODO: Check time value for a pattern match
	// Set next time section
	this.di_container.get("state").setSectionsTime(data.value);
	// Emit success event for admin
	socket.emit("admin-set-sections-time-success");
	
	logger.info("Server", "Sections time updated, new value is \"%s\", socket id %s", data.value, socket.id);
};