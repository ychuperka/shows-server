var
	ErrorCodes = require("../../modules/error_codes"),
	AdminEventSetSectionsTime = require("./set_sections_time");
	
/**
 * 
 * @param di_container {DICointainer}
 * @param socket socket.io socket
 * @param data {Object}
 * 
 * @constructor
 */
function AdminEventSetShowsTime(di_container, socket, data) {
	AdminEventSetSectionsTime.apply(this, arguments);
}
AdminEventSetShowsTime.prototype = Object.create(AdminEventSetSectionsTime.prototype);
AdminEventSetShowsTime.prototype.constructor = AdminEventSetShowsTime;
module.exports = AdminEventSetShowsTime;

AdminEventSetShowsTime.prototype.run = function () {
	var
		logger = this.di_container.get("logger"),
		socket = this.socket,
		data = this.data;
	// Check request empty
	if (AdminEventSetSectionsTime.prototype._emitEventIfRequestEmpty.call(this, "admin-set-shows-time-failed")) {
		return;
	}	
    // Check admin logged in
    if (AdminEventSetSectionsTime.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-set-shows-time-failed")) {
        logger.warn("Server", "Unauthorized attempt to set show time, socket id %s", socket.id);
        return;
    }	
	// Check time value specified
	if (!data.hasOwnProperty("value")) {
		logger.warn("Server", "Time value not specified, socket id %s", socket.id);
		socket.emit("admin-set-shows-time-failed", {
			error: {
				code: ErrorCodes.BAD_REQUEST,
				message: "Time value not specified"
			}
		});
		return;
	}
	// TODO: Check time value match to pattern
	// Set next show time
	this.di_container.get("state").setShowsTime(data.value);
	// Emit success event for admin
	socket.emit("admin-set-shows-time-success");
	
	logger.info("Server", "Shows time updated, new value id \"%s\", socket id %s", data.value, socket.id);
};