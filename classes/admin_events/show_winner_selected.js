var
    ErrorCodes = require("../../modules/error_codes"),
    ClientEventVote = require("../client_events/vote"),
    AdminEvent = require("../admin_event");

/**
 * 
 * @param di_container {DICointainer}
 * @param socket socket.io socket
 * @param data {Object}
 * 
 * @constructor
 */
function AdminEventShowWinnerSelected(di_container, socket, data) {
    AdminEvent.apply(this, arguments);
}
AdminEventShowWinnerSelected.prototype = Object.create(AdminEvent.prototype);
AdminEventShowWinnerSelected.prototype.constructor = AdminEventShowWinnerSelected;
module.exports = AdminEventShowWinnerSelected;

AdminEventShowWinnerSelected.prototype.run = function () {
    var
        instance = this,
        data = this.data,
        socket = this.socket,
        history = this.di_container.get("history"),
        mysql_connection = this.di_container.get("mysql_connection"),
        shows_places = this.di_container.get("shows_places"),
        logger = this.di_container.get("logger");
    // Check request empty
    if (AdminEvent.prototype._emitEventIfRequestEmpty.call(this, "admin-show-winner-selected-failed")) {
        return;
    }
    // Check admin logged in
    if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-show-winner-selected-failed")) {
        logger.warn("Server", "Unauthorized attempt to stop show, socket id %s", socket.id);
        return;
    }
    // Check places list
    if (shows_places.length == 0) {
        logger.error("Server", "Places list is empty, socket id %s", socket.id);
        socket.emit("admin-show-winner-selected-failed", {
            error: {
                code: ErrorCodes.INTERNAL_ERROR,
                message: "Places list is empty"
            }
        });
        return;
    }
    // Check place specified
    if (!data.hasOwnProperty("place")) {
        logger.warn("Server", "Place not specified, socket id %s", socket.id);
        socket.emit("admin-show-winner-selected-failed", {
            error: {
                code: ErrorCodes.BAD_REQUEST,
                message: "You should specify place"
            }
        });
        return;
    }
    // Check place is numeric
    var place = parseInt(data.place);
    if (isNaN(place)) {
        logger.warn("Server", "Place is not a number, socket id %s", socket.id);
        socket.emit("admin-show-winner-selected-failed", {
            error: {
                code: ErrorCodes.BAD_REQUEST,
                message: "Invalid place value"
            }
        });
        return;
    }
    // Check place is in valid range
    if (place < 1 || place > 3) {
        logger.warn("Server", "Place is in invalid range, socket id %s", socket.id);
        socket.emit("admin-show-winner-selected-failed", {
            error: {
                code: ErrorCodes.BAD_REQUEST,
                message: "Place is in invalid range"
            }
        });
        return;
    }
    // Get place data
    place--;
    if ((typeof shows_places[place]) === "undefined") {
        logger.warn("Server", "Place data not found, socket id %s", socket.id);
        socket.emit("admin-show-winner-selected-failed", {
            error: {
                code: ErrorCodes.NOT_FOUND,
                message: "Place data not found"
            }
        });
        return;
    }
    // Get show data
    var show_id = shows_places[place].id,
        show_place = place + 1;
    history.callIfRecordExists(history.GROUP.SHOWS, show_id, function (record_id) {
        // Select votes data
        mysql_connection.query(
            "SELECT COUNT(`id`) AS `count`, `type` FROM `vote` WHERE `show_id` = ? GROUP BY `type`",
            [show_id],
            function (err, results) {
                if (err) {
                    logger.error("Server", "Can`t select votes data, error code \"%s\", message: %s", err.code, err.message);
                    socket.emit("admin-show-winner-selected-failed", {
                        error: {
                            code: ErrorCodes.INTERNAL_ERROR,
                            message: err.message
                        }
                    });
                    return;
                }
                // Prepare votes data
                var votes_data = {};
                results.forEach(function (row) {
                    var key = null;
                    switch (row.type) {
                        default:
                            logger.warn("Server", "Unknown vote type \"%s\"", row.type);
                            break;
                        case ClientEventVote.prototype.VOTE_TYPE.GOOD:
                            key = "good";
                            break;
                        case ClientEventVote.prototype.VOTE_TYPE.VERY_GOOD:
                            key = "very_good";
                            break;
                        case ClientEventVote.prototype.VOTE_TYPE.EXCELLENT:
                            key = "excellent";
                            break;
                    }
                    if (key === null) {
                        return;
                    }
                    votes_data[key] = row.count;
                });
                // Emit event on all clients
                var payload = {
                    "id": show_id,
                    "place": show_place,
                    votes: [
                        votes_data.hasOwnProperty("good") ? votes_data.good : 0,
                        votes_data.hasOwnProperty("very_good") ? votes_data.very_good : 0,
                        votes_data.hasOwnProperty("excellent") ? votes_data.excellent : 0
                    ]
                };
                instance.di_container.get("io_ns_client")
                    .emit("show-winner-selected", payload);
                // Emit success event for admin    
                socket.emit("admin-show-winner-selected-success", payload);
            }
            );
    });
};