var
    ErrorCodes = require("../../modules/error_codes"),
    ClientEventVote = require("../client_events/vote"),
    AdminEvent = require("../admin_event");

function AdminEventSetVoteTypesWeight(di_container, socket, data) {
    AdminEvent.apply(this, arguments);
}
AdminEventSetVoteTypesWeight.prototype = Object.create(AdminEvent.prototype);
AdminEventSetVoteTypesWeight.prototype.constructor = AdminEventSetVoteTypesWeight;
module.exports = AdminEventSetVoteTypesWeight;

AdminEventSetVoteTypesWeight.prototype.run = function () {
    var
        data = this.data,
        socket = this.socket,
        state = this.di_container.get("state"),
        logger = this.di_container.get("logger");
    // Check request empty
    if (AdminEvent.prototype._emitEventIfRequestEmpty.call(this, "admin-set-vote-types-weight-failed")) {
        return;
    }
    // Check admin logged in
    if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-set-vote-types-weight-failed")) {
        logger.warn("Server", "Unauthorized attempt to set vote types weights, socket id %s", socket.id);
        return;
    }
    // Check values list specified
    if (!data.hasOwnProperty("values")) {
        logger.warn("Server", "Values list not specified, socket %s", socket.id);
        socket.emit("admin-set-vote-types-weight-failed", {
            error: {
                code: ErrorCodes.BAD_REQUEST,
                message: "Values list not specified"
            }
        });
        return;
    }
    // Process values list
    for (var vote_type in data.values) {
        var key = null;
        switch (vote_type) {
            case "good":
                key = ClientEventVote.prototype.VOTE_TYPE.GOOD;
                break;
            case "very_good":
                key = ClientEventVote.prototype.VOTE_TYPE.VERY_GOOD;
                break;
            case "excellent":
                key = ClientEventVote.prototype.VOTE_TYPE.EXCELLENT;
                break;
        }
        if (key === null) {
            continue;
        }
        var weight_value = parseInt(data.values[vote_type]);
        if (isNaN(weight_value)) {
            logger.warn("Server", "Invalid weight value, socket id %s", socket.id);
            socket.emit("admin-set-vote-types-weight-failed", {
                error: {
                    code: ErrorCodes.BAD_REQUEST,
                    message: "Invalid weight value for vote type \"" + vote_type + "\""
                }
            });
            return;
        }
        // Set weight value        
        try {
            state.setVoteTypeWeight(key, weight_value);
        } catch (e) {
            logger.warn("Server", "Can`t set vote type weight value, error: %s, socket id %s", e, socket.id);
            socket.emit("admin-set-vote-types-weight-failed", {
                error: {
                    code: ErrorCodes.BAD_REQUEST,
                    message: "Invalid weight value for vote type \"" + vote_type + "\", error: " + e
                }
            });
            return;
        }
    }
    // Save weight values
    state.saveVoteTypesWeights({
        success: function () {
            socket.emit("admin-set-vote-types-weight-success");
        },
        failed: function (err) {
            socket.emit("admin-set-vote-types-weight-failed", {
                error: {
                    code: ErrorCodes.INTERNAL_ERROR,
                    message: err.message
                }
            });
        }
    });
};