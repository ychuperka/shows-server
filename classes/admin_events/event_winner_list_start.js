var
	ErrorCodes = require("../../modules/error_codes"),
	AdminEvent = require("../admin_event");
	
/**
 * 
 * @param di_container {DICointainer}
 * @param socket socket.io socket
 * 
 * @constructor
 */
function AdminEventWinnerListStart(di_container, socket) {
	AdminEvent.apply(this, arguments);
}
AdminEventWinnerListStart.prototype = Object.create(AdminEvent.prototype);
AdminEventWinnerListStart.prototype.constructor = AdminEventWinnerListStart;
module.exports = AdminEventWinnerListStart;

AdminEventWinnerListStart.prototype.run = function () {
	var logger = this.di_container.get("logger"),
		socket = this.socket;
	// Check admin logged in
	if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-event-winner-list-start-failed")) {
		logger.warn("Server", "Unauthorized attempt to start winners list, socket id %s", socket.id);
		return;
	}
	// Emit event on all connected clients
	this.di_container.get("io_ns_client").emit("event-winner-list-start");
	// Emit success event for admin
	socket.emit("admin-event-winner-list-start-success");
    
    // Set state
    this.di_container.get("state")
        .setState("places-started");
	
	logger.info("Winners list started");
};