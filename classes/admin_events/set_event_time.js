var
	ErrorCodes = require("../../modules/error_codes"),
	AdminEvent = require("../admin_event");
	
/**
 * 
 * @param di_container {DICointainer}
 * @param socket socket.io socket
 * @param data {Object}
 * 
 * @constructor
 */
function AdminEventSetEventTime(di_container, socket, data) {
	AdminEvent.apply(this, arguments);
}
AdminEventSetEventTime.prototype = Object.create(AdminEvent.prototype);
AdminEventSetEventTime.prototype.constructor = AdminEventSetEventTime;
module.exports = AdminEventSetEventTime;

AdminEventSetEventTime.prototype.run = function () {
	var
		logger = this.di_container.get("logger"),
		socket = this.socket,
		data = this.data;
	// Check request empty
	if (AdminEvent.prototype._emitEventIfRequestEmpty.call(this, "admin-set-event-time-failed")) {
		return;
	}	
    // Check admin logged in
    if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-set-event-time-failed")) {
        logger.warn("Server", "Unauthorized attempt to set event time, socket id %s", socket.id);
        return;
    }
	// Check time value specified
	if (!data.hasOwnProperty("value")) {
		logger.warn("Server", "Time value not specified, socket id %s", socket.id);
		socket.emit("admin-set-event-time-failed", {
			error: ErrorCodes.BAD_REQUEST,
			message: "Time value not specified"
		});
		return;
	}
	// TODO: Check time value for a pattern match
	// Set next time section
	this.di_container.get("state").setEventTime(data.value);
	// Emit success event for admin
	socket.emit("admin-set-event-time-success");
    
    // Set state
    this.di_container.get("state")
        .setState("timer");
	
	logger.info("Server", "Event time updated, new value is \"%s\", socket id %s", data.value, socket.id);
};