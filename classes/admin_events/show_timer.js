var
    ErrorCodes = require("../../modules/error_codes"),
    AdminEvent = require("../admin_event");

/**
 * 
 * @param di_container {DICointainer}
 * @param socket socket.io socket
 * @param data {Object}
 * 
 * @constructor
 */
function AdminEventShowTimer(di_container, socket, data) {
    AdminEvent.apply(this, arguments);
}
AdminEventShowTimer.prototype = Object.create(AdminEvent.prototype);
AdminEventShowTimer.prototype.constructor = AdminEventShowTimer;
module.exports = AdminEventShowTimer;

AdminEventShowTimer.prototype.run = function () {
    var
        socket = this.socket,
        logger = this.di_container.get("logger");
    // Check admin logged in
    if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-show-timer-start-failed")) {
        logger.warn("Server", "Unauthorized attempt to start show timer, socket id %s", socket.id);
        return;
    }
    // Emit event on all connected clients
    this.di_container.get("io_ns_client")
        .emit("show-timer-start");
    // Emit success event for admin
    socket.emit("admin-show-timer-start-success");
    
    // Set state
    this.di_container.get("state")
        .setState("shows-timer");
    
    logger.info("Server", "Show timer started, socket id %s", socket.id);     
};