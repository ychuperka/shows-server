var
    AdminEvent = require("../admin_event"),
    ErrorCodes = require("../../modules/error_codes");
	
/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io socket
 * 
 * @constructor
 */
function AdminEventSync(di_container, socket) {
    AdminEvent.apply(this, arguments);
}
AdminEventSync.prototype = Object.create(AdminEvent.prototype);
AdminEventSync.prototype.constructor = AdminEventSync;
module.exports = AdminEventSync;

AdminEventSync.prototype.run = function (check_auth) {
    if ((typeof check_auth) === "undefined") {
        check_auth = true;
    }
    
    var
        self = this,
        state = this.di_container.get("state"),
        logger = this.di_container.get("logger");
    
    // Check admin logged in
    if (check_auth && AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-sync-failed")) {
        logger.warn("Server", "Unauthorized attempt to sync from admin");
        return;
    }
    
    // Send data
    logger.verbose("Server", "Sync data with admin");
    this.di_container.get("history").toObject(function (history) {
        self._getRegisteredUsersCount(function (users_count) {
            self.socket.emit("admin-sync-success", {
                state: state.getState(),
                values: {
                    server: (new Date).getTime(),
                    section: state.getSectionsTime(),
                    show: state.getShowsTime(),
                    event: state.getEventTime(),
                    update: state.getLastContentUpdateTime(),
                    "users_count": users_count
                },
                events: {
                    "history": history,
                    current: state.getCurrentEvent()
                },
            });
        });
    });
};

/**
 * Get registerd users count
 * 
 * @param {Callback} cb
 */
AdminEventSync.prototype._getRegisteredUsersCount = function (cb) {
    var
        mysql_connection = this.di_container.get("mysql_connection");
    mysql_connection.query("SELECT COUNT(`id`) AS `count` FROM `user`", function (err, results) {
        if (err) {
            AdminEvent.prototype._emitDbErrorEvent("admin-sync-failed", err);
            return;
        }
        cb(results[0].count);
    });
};