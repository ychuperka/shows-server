var
    ErrorCodes = require("../../modules/error_codes"),
    AdminEvent = require("../admin_event");
    
/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io socket
 * @param data {Object}
 * 
 * @constructor
 */
function AdminEventSectionsTimerStart(di_container, socket, data) {
    AdminEvent.apply(this, arguments);
}
AdminEventSectionsTimerStart.prototype = Object.create(AdminEvent.prototype);
AdminEventSectionsTimerStart.prototype.constructor = AdminEventSectionsTimerStart;
module.exports = AdminEventSectionsTimerStart;

AdminEventSectionsTimerStart.prototype.run = function () {
    // Check admin logged in
    if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-sections-timer-start-failed")) {
        this.di_container.get("logger")
            .warn("Server", "Unauthorized attempt to start sections timer, socket id %s", this.socket.id);
        return;
    }
    // Emit event on all connected clients
    this.di_container.get("io_ns_client")
        .emit("sections-timer-start");
    // Emit success event for admin
    this.socket.emit("admin-sections-timer-start-success");
    
    // Set state
    this.di_container.get("state")
        .setState("sections-timer");
    
    this.di_container.get("logger")
        .info("Server", "Sections timer srated, socket id %s", this.socket.id);
};