var
	ErrorCodes = require("../../modules/error_codes"),
	AdminEvent = require("../admin_event");
	
/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io socket
 * 
 * @constructor
 */
function AdminEventStopAllShows(di_container, socket) {
	AdminEvent.apply(this, arguments);
}
AdminEventStopAllShows.prototype = Object.create(AdminEvent.prototype);
AdminEventStopAllShows.prototype.constructor = AdminEventStopAllShows;
module.exports = AdminEventStopAllShows;

AdminEventStopAllShows.prototype.run = function () {
    var
        logger = this.di_container.get("logger");
     // Check admin logged in
	if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-event-stop-all-shows-failed")) {
		logger.warn("Server", "Unauthorized attempt to stop shows block, socket id %s", this.socket.id);
		return;
	}
    // Emit event on all connected clients
    this.di_container.get("io_ns_client")
        .emit("event-stop-all-shows");
    // Emit success event for admin
    this.socket.emit("admin-event-stop-all-shows-success");
    
    // Set state
    this.di_container.get("state")
        .setState("places-over");
    
    logger.info("Server", "Shows block stopped");     
};