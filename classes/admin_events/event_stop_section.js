var
    ErrorCodes = require("../../modules/error_codes"),
    AdminEvent = require("../admin_event");
	
/**
 * 
 * @param di_container {DICointainer}
 * @param socket socket.io socket
 * @param data {Object}
 * 
 * @constructor
 */
function AdminEventStopSection(di_container, socket, data) {
    AdminEvent.apply(this, arguments);
}
AdminEventStopSection.prototype = Object.create(AdminEvent.prototype);
AdminEventStopSection.prototype.constructor = AdminEventStopSection;
module.exports = AdminEventStopSection;

AdminEventStopSection.prototype.run = function () {
    var logger = this.di_container.get("logger"),
        data = this.data,
        state = this.di_container.get("state"),
        content = this.di_container.get("content");
    // Check request not empty
    if (AdminEvent.prototype._emitEventIfRequestEmpty.call(this, "admin-event-stop-section-failed")) {
        return;
    }	
    // Check admin logged in
    if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-event-stop-section-failed")) {
        logger.warn("Server", "Unauthorized attempt to stop secion, socket id %s", this.socket.id);
        return;
    }	
    // Check the section id is specified
    if (!data.hasOwnProperty("id")) {
        logger.warn(
            "Server", "Attempt to stop section, but id is not specified, socket id %s",
            this.socket.id
            );
        this.socket.emit("admin-event-stop-section-failed", {
            error: ErrorCodes.BAD_REQUEST,
            message: "You should send \"id\""
        });
        return;
    }
    // Check specified section id equal current active section id
    /*
    var current_event = state.getCurrentEvent();
    if (current_event.type !== "section" || current_event.entity_id != data.id) {
        logger.warn(
            "Server", "Attempt to stop innactive section, socket id %s",
            this.socket.id
            );
        this.socket.emit("admin-event-stop-section-failed", {
            error: {
                code: ErrorCodes.ACCESS_DENIED,
                message: "You trying to stop innactive section"
            }
        });
        return;
    }*/
    // Get section
    var section = null;
    content.sections.every(function (item) {
        if (item.id == data.id) {
            section = item;
            return false;
        }
        return true;
    });
    if (!section) {
        logger.error("Server", "Section #%s not found, socket id %s", data.id, this.socket.id);
        this.socket.emit("admin-event-stop-section-failed", {
            error: {
                code: ErrorCodes.INTERNAL_ERROR,
                message: "Section #" + data.id + " not found"
            }
        });
        return;
    }
    // Emit event on all connected clients
    this.di_container.get("io_ns_client").emit("event-stop-section", {
        id: data.id
    });
    // Emit success event for admin
    this.socket.emit("admin-event-stop-section-success", {
        id: data.id
    });
    // Mark history item as stopped
    var history = this.di_container.get("history");
    history.markAsStopped(history.GROUP.SECTIONS, data.id);
    // Set winner id
    history.setWinnerId(data.id, section.winner);
    // Erase current event
    this.di_container.get("state").setCurrentEvent(null, null);

    logger.info("Server", "Section \"%s\" successfully stopped, socket id %s", data.id, this.socket.id);
};