var
    ErrorCodes = require("../../modules/error_codes"),
    AdminEvent = require("../admin_event");
	
/**
 * 
 * @param di_container {DICointainer}
 * @param socket socket.io socket
 * @param data {Object}
 * 
 * @constructor
 */
function AdminEventStopShow(di_container, socket, data) {
    AdminEvent.apply(this, arguments);
}
AdminEventStopShow.prototype = Object.create(AdminEvent.prototype);
AdminEventStopShow.prototype.constructor = AdminEventStopShow;
module.exports = AdminEventStopShow;

AdminEventStopShow.prototype.run = function () {
    var logger = this.di_container.get("logger"),
        state = this.di_container.get("state"),
        mysql_connection = this.di_container.get("mysql_connection"),
        socket = this.socket,
        data = this.data,
        self = this;
    // Check request empty
    if (AdminEvent.prototype._emitEventIfRequestEmpty.call(this, "admin-event-stop-show-failed")) {
        return;
    }
    // Check admin logged in
    if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-event-stop-show-failed")) {
        logger.warn("Server", "Unauthorized attempt to stop show, socket id %s", socket.id);
        return;
    }
    // Check show id is specified
    if (!data.hasOwnProperty("id")) {
        logger.warn("Server", "Show id is not specified, socket id %s", socket.id);
        socket.emit("admin-event-stop-show-failed", {
            error: {
                code: ErrorCodes.BAD_REQUEST,
                message: "Show id is not specified"
            }
        });
        return;
    }
    /*
    // Check specified show id equal current active show id
    var current_event = state.getCurrentEvent();
    if (current_event.type !== "show" || current_event.entity_id !== data.id) {
        logger.warn("Server", "Specified show id not equal to current active show id, socket id %s", socket.id);
        socket.emit("admin-event-stop-show-failed", {
            error: {
                code: ErrorCodes.ACCESS_DENIED,
                message: "Specified show id not equal to current actuve show id"
            }
        });
        return;
    }
    */
    // Select votes
    mysql_connection.query(
        "SELECT COUNT(`id`) AS `count`, `type` FROM `vote` WHERE `show_id` = ? GROUP BY `type` ORDER BY `type`",
        [data.id],
        function (err, results) {
            if (err) {
                self._emitDbErrorEvent("admin-event-stop-show-failed", err);
                return;
            }
            var votes = [0, 0, 0];
            results.forEach(function (item) {
                votes[item.type] = item.count;
            });
            // Emit event on all connected clients
            self.di_container.get("io_ns_client").emit("event-stop-show", {
                id: data.id
            });
            // Emit success event for admin
            socket.emit("admin-event-stop-show-success", {
                id: data.id,
                "votes": votes
            });
            // Set history item data
            var history = self.di_container.get("history");
            history.markAsStopped(history.GROUP.SHOWS, data.id);
            // Set current event
            self.di_container.get("state").setCurrentEvent("show", data.id, "over");

            logger.info("Server", "Show \"%s\" successfully stopped, socket id %s", data.id, socket.id);
        }
        );
};