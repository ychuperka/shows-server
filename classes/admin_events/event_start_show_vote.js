var
    ErrorCodes = require("../../modules/error_codes"),
    AdminEventStartShow = require("../admin_events/event_start_show");
	
/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io socket
 * 
 * @constructor
 */
function AdminEventStartShowVote(di_container, socket, data) {
    AdminEventStartShow.apply(this, arguments);
}
AdminEventStartShowVote.prototype = Object.create(AdminEventStartShow.prototype);
AdminEventStartShowVote.prototype.constructor = AdminEventStartShowVote;
module.exports = AdminEventStartShowVote;

AdminEventStartShowVote.prototype.run = function () {
    var
        instance = this,
        data = this.data,
        logger = this.di_container.get("logger"),
        state = this.di_container.get("state"),
        socket = this.socket;
    AdminEventStartShow.prototype._callIfShowExists.call(
        this, "admin-event-start-show-vote-failed", function () {
            // Show found, emit event on all connected clients
            instance.di_container.get("io_ns_client").emit("event-start-show-vote", {
                id: data.id
            });
            // Emit success event for admin
            socket.emit("admin-event-start-show-vote-success", {
                id: data.id
            });
            // Set current event
            state.setCurrentEvent("show", data.id, "voting");

            logger.info("Server", "Vote for show \"%s\" successfully started, socket id %s", data.id, socket.id);
        });
};