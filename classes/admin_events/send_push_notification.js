var
    ErrorCodes = require("../../modules/error_codes"),
    AdminEvent = require("../admin_event"),
    https = require("https");

/**
 * 
 * @param di_container {DICointainer}
 * @param socket socket.io socket
 * @param data {Object}
 * 
 * @constructor
 */
function AdminEventSendPushNotification(di_container, socket, data) {
    AdminEvent.apply(this, arguments);
}
AdminEventSendPushNotification.prototype = Object.create(AdminEvent.prototype);
AdminEventSendPushNotification.prototype.constructor = AdminEventSendPushNotification;
module.exports = AdminEventSendPushNotification;

AdminEventSendPushNotification.prototype.run = function () {
    var
        data = this.data,
        socket = this.socket,
        config = this.di_container.get("config"),
        mysql_connection = this.di_container.get("mysql_connection"),
        logger = this.di_container.get("logger");
    // Check request not empty
    if (AdminEvent.prototype._emitEventIfRequestEmpty.call(this, "admin-send-push-notification-failed")) {
        return;
    }
    // Check admin logged in
    if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-send-push-notification-failed")) {
        logger.warn("Server", "Unauthorized attempt to send push notification, socket id %s", socket.id);
        return;
    }
    logger.info("Server", "Trying to send push notification, socket id %s", socket.id);        
    // Check text specified
    if (!data.hasOwnProperty("text")) {
        logger.warn("Server", "Text not specified, socket id %s", socket.id);
        socket.emit("admin-send-push-notification-failed", {
            error: {
                code: ErrorCodes.BAD_REQUEST,
                message: "Text not specified"
            }
        });
        return;
    }	
    // Select tokens
    mysql_connection.query(
        "SELECT `token` FROM `user` WHERE `token` IS NOT NULL",
        function (err, results) {
            if (err) {
                logger.error("Server", "Can`t select ionic tokens, error code: %s, message: %s", err.code, err.message);
                socket.emit("admin-send-push-notification-failed", {
                    error: {
                        code: ErrorCodes.INTERNAL_ERROR,
                        message: err.message
                    }
                });
                return;
            }
            // Check results list empty
            if (results.length == 0) {
                logger.warn("Server", "Can`t send push notification because tokens list is empty, socket id %s", socket.id);
                socket.emit("admin-send-push-notification-failed", {
                    error: {
                        code: ErrorCodes.INTERNAL_ERROR,
                        message: "Tokens list is empty"
                    }
                });
                return;
            }
            // Prepare request data
            var request_data = {
                tokens: [],
                notification: {
                    alert: data.text,
                    ios: {
                        sound: "ping.aiff"
                    }
                }
            };
            results.forEach(function (row) {
                request_data.tokens.push(row.token);
            });
            // Make a request
            var post_data = JSON.stringify(request_data);
            var request_options = {
                host: "push.ionic.io",
                path: "/api/v1/push",
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "X-Ionic-Application-Id": config.app.ionic_app_id,
                    "Authorization": "Basic " + (new Buffer(config.app.ionic_app_secret_key)).toString("base64") + ":"
                }
            };
            var response_body = "";
            var req = https.request(request_options, function (res) {
                // Check response code
                logger.info("Server", "Ionic response status code: %s", res.statusCode);
                if (res.statusCode != 202) {
                    // Bad response code (not 202 - Accepted)
                    logger.warn("Server", "Bad status code \"%s\" from Ionic", res.statusCode);
                    socket.emit("admin-send-push-notification-failed", {
                        error: {
                            code: ErrorCodes.INTERNAL_ERROR,
                            message: "Bad status code from Ionic: " + res.statusCode
                        }
                    });
                    return;
                }
                res.setEncoding("utf8");
                res.on("data", function (chunk) {
                    response_body += chunk;
                });
                res.on("end", function () {
                    // Parse server response
                    logger.info("Server", "Ionic server response: %s", response_body);
                    response_body = JSON.parse(response_body);
                    if (response_body.result != "queued") {
                        // Bad result (not queued)
                        logger.warn("Server", "Bad result \"%s\" from Ionic", response_body.result);
                        socket.emit("admin-send-push-notification-failed", {
                            error: {
                                code: ErrorCodes.INTERNAL_ERROR,
                                message: "Bad result Ionic: " + response_body.result
                            }
                        });
                        return;
                    }
                    // All ok
                    socket.emit("admin-send-push-notification-success", response_body);
                });
            });
            req.write(post_data);
            req.end();
        }
        );
};