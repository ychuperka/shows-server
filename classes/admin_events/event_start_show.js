var
    ErrorCodes = require("../../modules/error_codes"),
    AdminEvent = require("../admin_event");
	
/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io socket
 * 
 * @constructor
 */
function AdminEventStartShow(di_container, socket, data) {
    AdminEvent.apply(this, arguments);
}
AdminEventStartShow.prototype = Object.create(AdminEvent.prototype);
AdminEventStartShow.prototype.constructor = AdminEventStartShow;
module.exports = AdminEventStartShow;

AdminEventStartShow.prototype._callIfShowExists = function (event_name_failed, cb) {
    if ((typeof cb) === "undefined") {
        return;
    }
    var
        socket = this.socket,
        logger = this.di_container.get("logger");
    // Check request not empty
    if (AdminEvent.prototype._emitEventIfRequestEmpty.call(this, "admin-event-start-section-failed")) {
        return;
    }		
    // Check admin logged in
    if (this._emitEventIfAdminIsNotLoggedIn("admin-event-start-show-failed")) {
        logger.warn("Server", "Unauthorized attempt to start show, socket id %s", socket.id);
        return;
    }
    // Check show id specified
    if (!this.data.hasOwnProperty("id")) {
        logger.warn("Server", "Show id is not specified, socket id %s", socket.id);
        socket.emit("admin-event-start-show-failed", {
            error: {
                code: ErrorCodes.BAD_REQUEST,
                message: "Show id is not specified"
            }
        });
        return;
    }
    // Check show list exists in content
    if (!this.di_container.get("content").hasOwnProperty("shows")) {
        logger.warn("Server", "Shows list not found in content, socket id %s", socket.id);
        socket.emit("admin-event-start-show-failed", {
            error: {
                code: ErrorCodes.INTERNAL_ERROR,
                message: "Shows list not found in content"
            }
        });
        return;
    }
    // Check show exists
    if (!AdminEvent.prototype._entityExists.call(this, this.di_container.get("content").shows, this.data.id)) {
        logger.warn("Server", "Show \"%s\" not found, socket id %s", this.data.id, socket.id);
        socket.emit("admin-event-start-show-failed", {
            error: {
                code: ErrorCodes.NOT_FOUND,
                message: "Show \"" + this.data.id + "\" not found"
            }
        });
        return;
    }
    // All ok, call callback
    cb();
};

AdminEventStartShow.prototype.run = function () {
    var
        data = this.data,
        instance = this,
        socket = this.socket,
        logger = this.di_container.get("logger");
    this._callIfShowExists("admin-event-start-show-failed", function () {
        // Show found, emit event on all connected clients
        instance.di_container.get("io_ns_client").emit("event-start-show", {
            id: data.id
        });
        // Emit success event for admin
        socket.emit("admin-event-start-show-success", {
            id: data.id
        });
        // Add history item
        var history = instance.di_container.get("history");
        history.add(history.GROUP.SHOWS, data.id);
        // Set current event
        instance.di_container.get("state").setCurrentEvent("show", data.id, "wait");

        logger.info("Server", "Show \"%s\" successfully started, socket id %s", data.id, socket.id);
    });
};