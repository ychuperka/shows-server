var
	ErrorCodes = require("../../modules/error_codes"),
	AdminEvent = require("../admin_event");
	
/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io socket
 * @param data {Object}
 * 
 * @constructor
 */
function AdminEventStartSection(di_container, socket, data) {
	AdminEvent.apply(this, arguments);
}
AdminEventStartSection.prototype = Object.create(AdminEvent.prototype);
AdminEventStartSection.prototype.constructor = AdminEventStartSection;
module.exports = AdminEventStartSection;

AdminEventStartSection.prototype.run = function () {
	var data = this.data,
		socket = this.socket,
		content = this.di_container.get("content"),
		logger = this.di_container.get("logger");	
	// Check request not empty
	if (AdminEvent.prototype._emitEventIfRequestEmpty.call(this, "admin-event-start-section-failed")) {
		return;
	}	
	// Check admin logged in
	if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-event-start-section-failed")) {
		logger.warn("Server", "Unauthorized attempt to start secion, socket id %s", this.socket.id);
		return;
	}
	// Check section id property exists
	if (!data.hasOwnProperty("id")) {
		logger.warn("Server", "Section id is not specified, socket id %s", socket.id);
		socket.emit("admin-event-start-section-failed", {
			error: {
				code: ErrorCodes.BAD_REQUEST,
				message: "Section id is not specified"
			}
		});
		return;
	}
	// Check section list exists
	if (!content.hasOwnProperty("sections")) {
		logger.warn("Server", "Sections list not found in content, socket id %s", socket.id);
		socket.emit("admin-event-start-section-failed", {
			error: {
				code: ErrorCodes.INTERNAL_ERROR,
				message: "Sections list not foun in content"
			}
		});
		return;
	}
	// Get section
    var section = null;
    content.sections.every(function (item) {
        if (item.id == data.id) {
            section = item;
            return false;
        }
        return true;
    });
    if (!section) {
        logger.error("Server", "Section #%s not found, socket id %s", data.id, this.socket.id);
        socket.emit("admin-event-start-section-failed", {
            error: {
                code: ErrorCodes.INTERNAL_ERROR,
                message: "Section #" + data.id + " not found"
            }
        });
        return;
    }
    // Check section winner specified
    if (!section.hasOwnProperty("winner")) {
        logger.error("Server", "Winner not specified, section #%s, socket id %s", data.id, this.socket.id);
        socket.emit("admin-event-start-section-failed", {
            error: {
                code: ErrorCodes.INTERNAL_ERROR,
                message: "Winner not specified, section #" + data.id
            }
        });
        return;
    }
    // Check nominees list exists
    if (!content.hasOwnProperty("nominees")) {
        logger.error(
            "Server", "Nominees list not found"
            );
        socket.emit("admin-event-start-section-failed", {
            error: {
                code: ErrorCodes.INTERNAL_ERROR,
                message: "Nominees list not found"
            }
        });
        return;
    }
    // Check nominee exists
    if (!AdminEvent.prototype._entityExists(content.nominees, section.winner)) {
        logger.warn(
            "Server", "Nominee \"%s\" not found", section.winner
            );
        socket.emit("admin-event-start-section-failed", {
            error: ErrorCodes.NOT_FOUND,
            message: "Nominee \"" + section.winner + "\" not found"
        });
        return;
    }
	// Section found, emit event on all connected clients
	this.di_container.get("io_ns_client").emit("event-start-section", {
		id: data.id
	});
	// Emit success event for current socket
	socket.emit("admin-event-start-section-success", {
		id: data.id
	});
	// Add history item
	var history = this.di_container.get("history");
	history.add(history.GROUP.SECTIONS, data.id);
	// Set current event
	this.di_container.get("state").setCurrentEvent("section", data.id);	
	
	logger.info("Server", "Section \"%s\" successfully started, socket id %s", data.id, socket.id);
}