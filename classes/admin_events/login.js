var
	crypto = require("crypto"),
	ErrorCodes = require("../../modules/error_codes"),
	AdminEvent = require("../admin_event");

/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io socket
 * @param data {Object}
 * @constructor
 */
function AdminEventLogin(di_container, socket, data) {
	AdminEvent.apply(this, arguments);
}
AdminEventLogin.prototype = Object.create(AdminEvent.prototype);
AdminEventLogin.prototype.constructor = AdminEventLogin;
module.exports = AdminEventLogin;

/**
 * 
 * @param cb Will be called if login success, socket id will be passed as single parameter
 */
AdminEventLogin.prototype.run = function (cb) {
	var logger = this.di_container.get("logger"),
		data = this.data,
		socket = this.socket,
		config = this.di_container.get("config");
	// Check request is not empty
	if (AdminEvent.prototype._emitEventIfRequestEmpty
		.call(this, "admin-login-failed")) {
		return;
	}
	// Check password is specified
	var error = null;
	if (!data.hasOwnProperty("password")) {
		error = {
			code: ErrorCodes.BAD_REQUEST,
			message: "A password is empty"
		};
		logger
			.warn(
				"Server", "Admin login failed, error code %s, message: %s",
				error.code,
				error.message
				);
		socket.emit("admin-login-failed", {
			"error": error
		});
		return;
	} 
	// Create hashed for received password and stored password and check they are match
	var stored_password_hash = crypto.createHash("sha1")
		.update(config.app.admin_password.toString())
		.digest("hex");
	if (data.password.toString() != stored_password_hash) {
		error = {
			code: ErrorCodes.ACCESS_DENIED,
			message: "Wrong password"
		};
		logger
			.warn(
				"Server", "Admin login failed, error code %s, message: %s",
				error.code,
				error.message
				);
		socket.emit("admin-login-failed", {
			"error": error
		});
		return;
	}
	// Checks passed
	logger.verbose("Server", "Admin successfully logged in, socket id: %s", socket.id);
	socket.emit("admin-login-success");
	cb(socket.id);
};