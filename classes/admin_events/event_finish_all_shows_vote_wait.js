var
    ErrorCodes = require("../../modules/error_codes"),
    ClientEventVote = require("../client_events/vote"),
    AdminEvent = require("../admin_event");
	
/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io socket
 * 
 * @constructor
 */
function AdminEventFinishAllShowsVoteWait(di_container, socket) {
    AdminEvent.apply(this, arguments);
}
AdminEventFinishAllShowsVoteWait.prototype = Object.create(AdminEvent.prototype);
AdminEventFinishAllShowsVoteWait.prototype.constructor = AdminEventFinishAllShowsVoteWait;
module.exports = AdminEventFinishAllShowsVoteWait;

AdminEventFinishAllShowsVoteWait.prototype.run = function () {
    var
        self = this,
        logger = this.di_container.get("logger");
    // Check admin logged in
    if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-event-finish-all-shows-vote-wait-failed")) {
        logger.warn("Server", "Unauthorized attempt to finish shows block, socket id %s", this.socket.id);
        return;
    }
    this._updateShowsPlaces(function (places) {
        // Put places to di container
        self.di_container.set("shows_places", places);
        // Emit event on all connected clients
        self.di_container.get("io_ns_client")
            .emit("event-finish-all-shows-vote-wait");
        // Emit success event for admin
        self.socket.emit("admin-event-finish-all-shows-vote-wait-success", {
            'places': places
        });
        // Set state
        self.di_container.get("state")
            .setState("shows-over");
        // Erase current event
        self.di_container.get("state").setCurrentEvent(null, null);

        logger.info("Server", "Shows block finished");
    });

};

/**
 * Update shows places
 * 
 * @param {Callback} success_db
 */
AdminEventFinishAllShowsVoteWait.prototype._updateShowsPlaces = function (success_cb) {
    var
        self = this,
        logger = self.di_container.get("logger"),
        mysql_connection = this.di_container.get("mysql_connection"),
        votes_data = {};
    // Get shows ids
    mysql_connection.query(
        "SELECT `show_id` FROM `vote`",
        function (err, results) {
            if (err) {
                AdminEvent.prototype._emitDbErrorEvent.call(
                    self, "admin-event-finish-all-shows-vote-wait-failed", err
                    );
                return;
            }
            // check results empty
            if (results.length == 0) {
                success_cb([]);
                return;
            }
            // for each row id do
            for (var i = 0; i < results.length; ++i) {
                /*
                 for each time we execute a db query,
                 we should check it is last, if it is last
                 then after processing query result we should
                 call logic to calculate places (O.M.G asynchronous programming)
                 */
                (function (is_last, show_id) {
                    mysql_connection.query(
                        "SELECT COUNT(`id`) AS `count`, `type` FROM `vote` WHERE `show_id` = ? GROUP BY `type`",
                        [show_id],
                        function (err, results) {
                            if (err) {
                                AdminEvent.prototype._emitDbErrorEvent.call(
                                    self, "admin-event-finish-all-shows-vote-wait-failed", err
                                    );
                                return;
                            }
                            // Read votes data to a memory
                            results.forEach(function (row) {
                                votes_data[show_id] = {};
                                votes_data[show_id][row.type] = row.count;
                            }); 
                            // If it is a last
                            if (is_last) {
                                var places = null;
                                try {
                                    places = self._calculateShowsPlaces(votes_data);
                                } catch (err) {
                                    logger.error("Server", "Error occur while trying to calculate shows places, message: " + err.message);
                                }
                                success_cb(places);
                            }
                        }
                        );
                })((i + 1) == results.length, results[i].show_id);
            }
        }
        );
};

/**
 * Calculcate shows places
 * 
 * @param {Object} votes_data
 * 
 * @return {Array}
 * @throws {Error}
 */
AdminEventFinishAllShowsVoteWait.prototype._calculateShowsPlaces = function (votes_data) {
    var shows_points = {},
        config = this.di_container.get("config");

    for (var show_id in votes_data) {

        for (var type in votes_data[show_id]) {

            for (var type_as_string in ClientEventVote.prototype.VOTE_TYPE) {

                if (ClientEventVote.prototype.VOTE_TYPE[type_as_string] == type) {
                    var type_as_lc_string = type_as_string.toLowerCase();
                    if (!config.app.vote_types_weights.hasOwnProperty(type_as_lc_string)) {
                        throw new Error("Vote weight for type \"" + type_as_lc_string + "\" not found");
                    }

                    var weight = config.app.vote_types_weights[type_as_lc_string];
                    var points = parseInt(votes_data[show_id][type]) * weight;

                    if (!shows_points.hasOwnProperty(show_id)) {
                        shows_points[show_id] = 0;
                    }
                    shows_points[show_id] += points;
                }
            }
        }
    }

    var places = [];
    for (var show_id in shows_points) {
        places.push({
            id: parseInt(show_id),
            points: shows_points[show_id]
        });
    }
    shows_points = null;

    places.sort(function (a, b) {
        if (a.points > b.points) {
            return -1;
        } else if (a.points < b.points) {
            return 1;
        } else {
            return 0;
        }
    });
    
    // Set places in history
    var history = this.di_container.get("history");
    places.forEach(function (show, index) {
        history.setPlace(show.id, index + 1);
    });

    return places.slice(0, 3);
};