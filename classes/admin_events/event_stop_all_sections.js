var
	ErrorCodes = require("../../modules/error_codes"),
	AdminEvent = require("../admin_event");
	
/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io socket
 * 
 * @constructor
 */
function AdminEventStopAllSections(di_container, socket) {
	AdminEvent.apply(this, arguments);
}
AdminEventStopAllSections.prototype = Object.create(AdminEvent.prototype);
AdminEventStopAllSections.prototype.constructor = AdminEventStopAllSections;
module.exports = AdminEventStopAllSections;

AdminEventStopAllSections.prototype.run = function () {
    var
        logger = this.di_container.get("logger");
     // Check admin logged in
	if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-event-stop-all-sections-failed")) {
		logger.warn("Server", "Unauthorized attempt to stop sections block, socket id %s", this.socket.id);
		return;
	}
    // Emit event on all connected clients
    this.di_container.get("io_ns_client")
        .emit("event-stop-all-sections");
    // Emit success event for admin
    this.socket.emit("admin-event-stop-all-sections-success");
    
    // Set state
    this.di_container.get("state")
        .setState("sections-over");
    
    logger.info("Server", "Sections block stopped");     
};