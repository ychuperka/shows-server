var
	ErrorCodes = require("../../modules/error_codes"),
	AdminEvent = require("../admin_event");
	
/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io socket
 * 
 * @constructor
 */
function AdminEventStartAllSections(di_container, socket) {
	AdminEvent.apply(this, arguments);
}
AdminEventStartAllSections.prototype = Object.create(AdminEvent.prototype);
AdminEventStartAllSections.prototype.constructor = AdminEventStartAllSections;
module.exports = AdminEventStartAllSections;

AdminEventStartAllSections.prototype.run = function () {
    var
        logger = this.di_container.get("logger");
     // Check admin logged in
	if (AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn.call(this, "admin-event-start-all-shows-failed")) {
		logger.warn("Server", "Unauthorized attempt to start shows block, socket id %s", this.socket.id);
		return;
	}
    // Emit event on all connected clients
    // this.di_container.get("io_ns_client")
    //     .emit("event-start-all-shows");
    // Emit success event for admin
    this.socket.emit("admin-event-start-all-shows-success");
    
    // Set state
    this.di_container.get("state")
        .setState("shows");
    
    logger.info("Server", "Shows block started");     
};