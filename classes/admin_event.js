var
	merge = require("merge"),
	ErrorCodes = require("../modules/error_codes"),
	ClientEvent = require("./client_event");
	
/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io
 * @param data {Object}
 * @constructor
 */
function AdminEvent(di_container, socket, data) {
	ClientEvent.apply(this, arguments);
}
AdminEvent.prototype = Object.create(ClientEvent.prototype);
AdminEvent.prototype.constructor = AdminEvent;
module.exports = AdminEvent;

/**
 * Emit event if admin is not logged in
 * Return true if event emitted else return false.
 * 
 * @param event_name
 * @return boolean
 */
AdminEvent.prototype._emitEventIfAdminIsNotLoggedIn = function (event_name) {
	var admin_socket_id = this.di_container.get("admin_socket_id");
	if (admin_socket_id.length == 0 || admin_socket_id != this.socket.id) {
		this.socket.emit(event_name, {
			code: ErrorCodes.ACCESS_DENIED,
			message: "You are not authorized"
		});
		return true;
	}
	return false;
};
