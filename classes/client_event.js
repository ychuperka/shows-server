var
	ErrorCodes = require("../modules/error_codes");

/**
 * 
 * @param di_container {DIContainer}
 * @param socket socket.io socket
 * @param data {Object}
 * @constructor
 */
function ClientEvent(di_container, socket, data) {
	this.di_container = di_container;
	this.socket = socket;
	this.data = data;
}
module.exports = ClientEvent;

/**
 * Emit db error event
 * 
 * @param event_name The name will be used to emit event on a client side
 * @param db_error Database error object
 */
ClientEvent.prototype._emitDbErrorEvent = function (event_name, db_error) {
	this.di_container.get("logger")
		.warn("Database", "Error occur, code \"%s\", stack: %s", db_error.code, db_error.stack);
	this.socket.emit(event_name, {
		"error": {
			"code": ErrorCodes.INTERNAL_ERROR,
			"message": db_error.code + "\n" + db_error.stack
		}
	});
};

/**
 * Emit event if request is empty
 * 
 * Return true if request empty else return false
 * 
 * @param event_name {String}
 * 
 * @return boolean
 */ 
ClientEvent.prototype._emitEventIfRequestEmpty = function (event_name) {
	if ((typeof this.data) == "undefined") {
		this.socket.emit(event_name, {
			error: {
				code: ErrorCodes.BAD_REQUEST,
				message: "Empty request"
			}
		});	
		this.di_container.get("logger")
			.warn("Server", "Empty request, socket id %s", this.socket.id);
		return true;
	}
	return false;
};

/**
 * Emit some event if passed value is undefined.
 * Return false is value is not undefined
 * else return true.
 *
 * @param event_name The name will be used to emit event on a client side
 * @param key 
 * @param value
 * 
 * @return boolean
 */
ClientEvent.prototype._emitEventIfValueUndefined = function (event_name, key, value) {
	if ((typeof value) !== "undefined") {
		return false;
	}
	this.di_container.get("logger")
		.verbose("Server", "Value for key \"%s\" is undefined", key);
	this.socket.emit(event_name, {
		error: {
			code: ErrorCodes.BAD_REQUEST,
			message: "Value for key \"" + key + "\" is undefined"
		}
	});
	return true;
};

/**
 * Check entity exists in a list
 * 
 * @param {Array} list
 * @param {Number} entity_ii
 */
ClientEvent.prototype._entityExists = function (list, entity_id) {
	var exists = false;
	for (var i = 0; i < list.length; ++i) {
		if (list[i].id == entity_id) {
			exists = true;
			break;
		}
	}
	return exists;
};

/**
 * Run the event
 * 
 */
ClientEvent.prototype.run = function (callbacks) {
	throw new Error("The method should be overriden");
};
