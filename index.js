"use strict";

/*
 Load modules
 */
var

    // Classes
    KVStorage = require("./classes/kvstorage"),
    DIContainer = require("./classes/di_container"),
    EventsHistory = require("./classes/events_history"),
    ClientEventUserRegistration = require("./classes/client_events/user_registration"),
    ClientEventVote = require("./classes/client_events/vote"),
    ClientEventSync = require("./classes/client_events/sync"),
    AdminEventLogin = require("./classes/admin_events/login"),
    AdminEventStartSection = require("./classes/admin_events/event_start_section"),
    AdminEventStopSection = require("./classes/admin_events/event_stop_section"),
    AdminEventStartShow = require("./classes/admin_events/event_start_show"),
    AdminEventStartShowVote = require("./classes/admin_events/event_start_show_vote"),
    AdminEventStopShow = require("./classes/admin_events/event_stop_show"),
    AdminEventSetSectionsTime = require("./classes/admin_events/set_sections_time"),
    AdminEventSetShowsTime = require("./classes/admin_events/set_shows_time"),
    AdminEventStartAllSections = require("./classes/admin_events/event_start_all_sections"),
    AdminEventStartAllShows = require("./classes/admin_events/event_start_all_shows"),
    AdminEventFinishAllShowsVoteWait = require("./classes/admin_events/event_finish_all_shows_vote_wait"),
    AdminEventStopAllSections = require("./classes/admin_events/event_stop_all_sections"),
    AdminEventStopAllShows = require("./classes/admin_events/event_stop_all_shows"),
    AdminEventWinnerListStart = require("./classes/admin_events/event_winner_list_start"),
    AdminEventShowWinnerSelected = require("./classes/admin_events/show_winner_selected"),
    AdminEventSendPushNotification = require("./classes/admin_events/send_push_notification"),
    AdminEventSetVoteTypesWeight = require("./classes/admin_events/set_vote_types_weight"),
    AdminEventShowTimer = require("./classes/admin_events/show_timer"),
    AdminEventSetEventTime = require("./classes/admin_events/set_event_time"),
    AdminEventSectionsTimerStart = require("./classes/admin_events/sections_timer_start"),
    AdminEventTruncateTables = require("./classes/admin_events/truncate_tables"),
    AdminEventSync = require("./classes/admin_events/sync"),
    AdminEventSyncClients = require("./classes/admin_events/sync_clients"),

    // Modules
    state = require("./modules/state"),
    errors = require("./modules/error_codes"),
    mysql = require("mysql"),
    fs = require("fs"),
    path = require("path"),
    http = require("http");
  
/*
Initialize di container 
*/
var di_container = new DIContainer;
di_container.set("admin_socket_id", "");
di_container.set("state", state);
di_container.set("history", new EventsHistory(di_container));
di_container.set("shows_places", []);
    
/*
 Initialize logger
 */
var logger = require("npmlog");
logger.heading = "shows-server";
logger.level = "silly";
logger.stream = process.stdout;
di_container.set("logger", logger);


/*
Load config
*/
var config = require("./modules/config")(logger, shutdown);
di_container.set("config", config);

// Set logger level obtained from app configuration
logger.level = config.app.logger_level;

// The variable stores content (sections, show, nominees, etc)
var content_file_path = path.resolve(__dirname, config.app.content_filename);
if (!fs.existsSync(content_file_path)) {
    logger.error("Server", "Content file \"%s\" not found", content_file_path);
    shutdown(1);
}
di_container.set("content", JSON.parse(fs.readFileSync(content_file_path)));
var contentWatcher = fs.watch(content_file_path, function (event, filename) {
    if (event !== "change") {
        return;
    }
    // Reload content if file changed
    logger.verbose("Server", "Content changed, reloading...");
    if (!fs.existsSync(content_file_path)) {
        logger.error("Server", "Content file is missing while trying to reload it, path \"%s\"", content_file_path);
        shutdown(1);
    }
    di_container.set("content", JSON.parse(fs.readFileSync(content_file_path)));
    // Update last content update time
    state.setLastContentUpdateTime(
        (new Date).getTime()
        );
});

/*
 Connect to the database server
 */
var mysql_connection = mysql.createConnection(config.db);
mysql_connection.connect(function (err) {

    if (err) {
        logger.error("Server", "Can`t connect to the database server, error code: %s, message: %s",
            err.code, err.message);
        shutdown(1);
        return;
    }

    logger.verbose("Server", "Connected to database server as id: %s", mysql_connection.threadId);
    di_container.set("mysql_connection", mysql_connection);
    
    /*
    Try to load state
    */
    logger.verbose("Server", "Loading state");
    state.load(mysql_connection, logger, {
        success: function () {
            // Set last content update time
            state.setLastContentUpdateTime((new Date).getTime());
            // Show current event info
            var current_event = state.getCurrentEvent();
            logger.info("Server", "Current event type \"%s\", current event id \"%s\"", current_event.type, current_event.entity_id);
            /*
            Initialize web server
            */
            var server = http.createServer(function (request, response) {
                response.writeHead(200, { "Content-Type": "text/plain" });
                response.end("Luke, I am your father!");
            }).listen(config.app.listen_port);
            logger.info("Server", "Listen on port: %s", config.app.listen_port);
            /*
            Initialize socket.io
            */
            var io = require("socket.io")(server);
            di_container.set("io", io);
            /*
            Set event handlers for clients
            */
            // Fires when a client connected
            var io_ns_client = io.of("/client");
            di_container.set("io_ns_client", io_ns_client);
            io_ns_client.on("connection", function (socket) {
                logger.verbose("Server", "New client connection, id: %s", socket.id);
                /*
                Set socket event handlers
                */
                // Fires when connection lost
                socket.on("disconnect", function () {
                    logger.verbose("Server", "Client \"%s\" disconnected", socket.id);
                });
                // Fires when user tries to register
                socket.on("user-register", function (data) {
                    (new ClientEventUserRegistration(di_container, socket, data))
                        .run();
                });
                // Fires when user send its vote
                socket.on("vote", function (data) {
                    (new ClientEventVote(di_container, socket, data))
                        .run();
                });
                // Fires when client tries to sync data
                socket.on("sync", function (data) {
                    (new ClientEventSync(di_container, socket, data))
                        .run();
                });
                // Fires when client tries to sync content
                socket.on("content-sync", function (data) {
                    socket.emit("content-sync", di_container.get("content"));
                });
            });

            /*
            Set event handlers form admin
            */
            var io_ns_admin = io.of("/admin");
            di_container.set("io_ns_admin", io_ns_admin);
            io_ns_admin.on("connection", function (socket) {
                logger.verbose("Server", "New admin connection, id: %s", socket.id);
                // Fires when connection lost
                socket.on("disconnect", function () {
                    logger.verbose("Server", "Admin \"%s\" disconnected", socket.id);
                });
                // Fires when admin tries to login
                socket.on("admin-login", function (data) {
                    (new AdminEventLogin(di_container, socket, data))
                        .run(function (socket_id) {
                            di_container.set("admin_socket_id", socket_id);
                        });
                });    
                // Fires when admin wants to start section
                socket.on("admin-event-start-section", function (data) {
                    (new AdminEventStartSection(di_container, socket, data))
                        .run();
                });
                // Fires when admin wants to start show
                socket.on("admin-event-start-show", function (data) {
                    (new AdminEventStartShow(di_container, socket, data))
                        .run();
                });           
                // Fires when admin wants to start vote
                socket.on("admin-event-start-show-vote", function (data) {
                    (new AdminEventStartShowVote(di_container, socket, data))
                        .run();
                });  
                // Fires when admin wants to stop section
                socket.on("admin-event-stop-section", function (data) {
                    (new AdminEventStopSection(di_container, socket, data))
                        .run();
                });        
                // Fires when admin wants to stop show
                socket.on("admin-event-stop-show", function (data) {
                    (new AdminEventStopShow(di_container, socket, data))
                        .run();
                });
                // Fires when admin wants to update sections time
                socket.on("admin-set-sections-time", function (data) {
                    (new AdminEventSetSectionsTime(di_container, socket, data))
                        .run();
                });
                // Fires when admin wants to update shows time
                socket.on("admin-set-shows-time", function (data) {
                    (new AdminEventSetShowsTime(di_container, socket, data))
                        .run();
                });
                // Fires when admin wants to start sections block
                socket.on("admin-event-start-all-sections", function () {
                    (new AdminEventStartAllSections(di_container, socket))
                        .run();
                });
                // Fires when admin wants to start shows block
                socket.on("admin-event-start-all-shows", function () {
                    (new AdminEventStartAllShows(di_container, socket))
                        .run();
                });
                // Fires when admin wants to stop shows block and enable "vote waiting" mode
                socket.on("admin-event-finish-all-shows-vote-wait", function () {
                    (new AdminEventFinishAllShowsVoteWait(di_container, socket))
                        .run();
                });
                // Fires when admin wants to stop sections block
                socket.on("admin-event-stop-all-sections", function () {
                    (new AdminEventStopAllSections(di_container, socket))
                        .run();
                });
                // Fires when admin wants to stop shows block
                socket.on("admin-event-stop-all-shows", function () {
                    (new AdminEventStopAllShows(di_container, socket))
                        .run();
                });
                // Fires when admin wants to start winners list
                socket.on("admin-event-winner-list-start", function () {
                    (new AdminEventWinnerListStart(di_container, socket))
                        .run();
                });
                // Fires when admin wants to show winner info
                socket.on("admin-show-winner-selected", function (data) {
                    (new AdminEventShowWinnerSelected(di_container, socket, data))
                        .run();
                });
                // Fires when admin wants to send push notification
                socket.on("admin-send-push-notification", function (data) {
                    (new AdminEventSendPushNotification(di_container, socket, data))
                        .run();
                });
                // Fires when admin wants to send vote types weight
                socket.on("admin-set-vote-types-weight", function (data) {
                    (new AdminEventSetVoteTypesWeight(di_container, socket, data))
                        .run();
                });
                // Fires when admin wants to show timer
                socket.on("admin-show-timer-start", function (data) {
                    (new AdminEventShowTimer(di_container, socket, data))
                        .run();
                });
                // Fires when admin tries to sync content
                socket.on("admin-content-sync", function (data) {
                    socket.emit("admin-content-sync", di_container.get("content"));
                });
                // Fires when admin tries to set event time
                socket.on("admin-set-event-time", function (data) {
                    (new AdminEventSetEventTime(di_container, socket, data))
                        .run();    
                });
                // Fires when admin tries to start sections timer
                socket.on("admin-sections-timer-start", function (data) {
                    (new AdminEventSectionsTimerStart(di_container, socket, data))
                        .run();
                });
                // Fires when admin tries to truncate tables
                socket.on("admin-truncate-tables", function () {
                    (new AdminEventTruncateTables(di_container, socket))
                        .run();
                });
                // Fires when admin wants to sync
                socket.on("admin-sync", function () {
                    (new AdminEventSync(di_container, socket))
                        .run();
                });
                // Fires when admin wants to sync all clients
                socket.on("admin-sync-clients", function () {
                    (new AdminEventSyncClients(di_container, socket))
                        .run();
                });
            });
        },
        failed: function (error) {
            logger.error("Server", "Can`t load state, error code: %s, message: %s", error.code, error.message);
            shutdown(1);
        }
    });
});

/**
 * The function being used to gracefully shutdown a program
 * 
 * @param code Exit code
 */
function shutdown(code) {
    logger.info("Server", "Terminating...");
    // Close content watcher
    if (contentWatcher) {
        contentWatcher.close();
    }
    // Close connection to mysql server
    if (mysql_connection) {
        mysql_connection.end(function (err) {
            if (!err) {
                process.exit(code);
                return;
            }
            logger
                .warn("Server", "Error while disconnecting from database server, error code %s, message: %s", err.code, err.message);
        });
    } else {
        process.exit(code);
    }
}
// Catch SIGINT
process.on("SIGINT", function () {
    shutdown(0);
});