DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(64) NOT NULL,
  `surname` VARCHAR(64) NOT NULL,
  `token` VARCHAR(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY (`name`),
  KEY (`surname`)
) ENGINE = InnoDB CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

/*
DROP TABLE IF EXISTS `nominee`;
CREATE TABLE `nominee` (
  `user_id` INT(11) UNSIGNED NOT NULL,
  `avatar_link` VARCHAR(255) DEFAULT NULL,
  `description` TEXT DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

DROP TABLE IF EXISTS `show`;
CREATE TABLE `show` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
  `state` TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY (`name`)
) ENGINE = InnoDB CHARACTER SET `utf8` COLLATE `utf8_general_ci`;
INSERT INTO `show` VALUES (NULL, 'Sphere show!', 0);


DROP TABLE IF EXISTS `nominee_to_show`;
CREATE TABLE `nominee_to_show` (
  `show_id` INT(11) UNSIGNED NOT NULL,
  `nominee_id` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`show_id`, `nominee_id`),
  FOREIGN KEY (`show_id`) REFERENCES `show` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`nominee_id`) REFERENCES `nominee` (`user_id`) ON DELETE CASCADE
) ENGINE = InnoDB CHARACTER SET `utf8` COLLATE `utf8_general_ci`;
*/

DROP TABLE IF EXISTS `vote`;
CREATE TABLE `vote` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `show_id` INT(11) UNSIGNED NOT NULL,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `type` TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)/*,
  FOREIGN KEY (`show_id`) REFERENCES `show` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE*/
) ENGINE = InnoDB CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

DROP TABLE IF EXISTS `kv_storage`;
CREATE TABLE `kv_storage` (
    `key` VARCHAR(64) NOT NULL,
    `value` TEXT DEFAULT NULL,
    `group` VARCHAR(64) NOT NULL, 
    PRIMARY KEY (`key`),
    KEY (`group`)
) ENGINE = InnoDB CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

DROP TABLE IF EXISTS `history`;
CREATE TABLE `history` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `group_id` TINYINT(1) UNSIGNED NOT NULL,
    `entity_id` INT(11) UNSIGNED NOT NULL,
    `started_at` DATETIME NOT NULL,
    `stopped_at` DATETIME DEFAULT NULL,
    `winner_id` INT(11) UNSIGNED DEFAULT NULL,
    `place` TINYINT(1) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY (`group_id`, `entity_id`),
    KEY (`started_at`),
    KEY (`stopped_at`),
    KEY (`started_at`, `stopped_at`)
) ENGINE = InnoDB CHARACTER SET `utf8` COLLATE `utf8_general_ci`;
